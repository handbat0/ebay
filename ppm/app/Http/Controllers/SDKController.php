<?php

namespace ppm\Http\Controllers;

use ppm\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use \DTS\eBaySDK\Constants;
use \DTS\eBaySDK\Trading\Services;
use \DTS\eBaySDK\Trading\Types;
use \DTS\eBaySDK\Trading\Enums;

use ppm\Models\Products\Product;
use ppm\Models\Products\Material;
use ppm\Models\RedBookCategory;

use ppm\GetEbayTime;
use ppm\CreateEbayProduct;
use ppm\EndEbayProduct;
use ppm\AllEbayProducts;
use ppm\FindEbayProduct;
use ppm\GetApiToken;
use ppm\EbayMarketplace;

use ppm\Models\Marketplaces\CoinCollector;

class SDKController extends Controller
{
    public function get_time()
    {
        //phpinfo();
        $ebay_keys = EbayMarketplace::where('type', 'eBay')->first();

            if (!($ebay_keys && $ebay_keys->api_token)) {
                return 0;
            }

        $appId  = $ebay_keys->app_id;
        $certId = $ebay_keys->cert_id;
        $devId  = $ebay_keys->dev_id;
        $eBayAuthToken = $ebay_keys->api_token;

        dd ( GetEbayTime::GetTime($appId, $certId, $devId, $eBayAuthToken) );
        AllEbayProducts::AllProducts();
        return view('SDK', ['CurrEbayTime' => GetEbayTime::GetTime($appId, $certId, $devId, $eBayAuthToken)]);
    }

    public function all_products()
    {
        return view('AllProducts', ['AllProducts' => AllEbayProducts::AllProducts()]);
    }

    public function create_product(Request $request)
    {
        $params = $request->input();
        unset($params['_token']);
        CreateEbayProduct::CreateProduct($params);
        return redirect('/get-time');
    }

    public function end_products(Request $request)
    {
        EndEbayProduct::EndProduct($request->input()['prod_id']);
        return redirect('/get-time');
    }

    public function getEbayCategories()
    {

        $appId  = "IvanKhan-aurestes-SBX-5dfe99f22-fc107df8";
        $certId = "SBX-dfe99f226946-43f4-40f5-8543-33ae";
        $devId  = "84ae2671-505c-4cdd-b5aa-ab1199939a47";
        $eBayAuthToken = "AgAAAA**AQAAAA**aAAAAA**/iBuXQ**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4aiCZeHpgSdj6x9nY+seQ**aRQFAA**AAMAAA**UahAH0s7ezCk2qp660rsrxonHgaUaOz9x7R7odLxn5RNIXJvy0He0smOQR+uOp/Jp0/ERC/+plhEcFmuxoUQPAY+jdLWUxWm9oK3QCjvLjwczts/BY2LCjud1giXTSFc991CJ0wIUSvc6i7IyIH69AdF1zVxv1ceQM1FwH1yQmdLjiLn8+3imvPdmtVesD0jzMZVZ3Y2olA05MhkHYx9RbVEuwDStI2THYC+IQ6ZhDdIYmATg6HxcjpAB1FXPyfBLi52NW5vkEzxZBMk7aRY8w8qERSWq4dEQ+raGWZcbCYlr6bIHXI8DRRBnilyDPB8nBPAIOdHCqLHHGLKM/LErON9e5sVXsoYfsYDrEdrSUDVtd4Qsne5rWrs1S5Qt3IfUptZ+4ouARNfOfecYoKETjRmbviBFAHne25tHsOtAUzKPpOCZtKXK6RxejoTMp1kOHmuxzMH47f4XfOmJJOABEux9tkl+lwmLpp7MtgonmlEkZa/tWtOBASMPlHb6aM1Au9lqGtfIk+3RkCDYTBDCOtHsaauhKx3kEbogikrQ1qFKilytEKIyZykr6I05erlaWBclw4GecZW1lhdzZhc0PMerTknTlQmEnhdshC4JthdOfHiLSZtjObVVcYdlD1maDbJ2Nlel1BAs6JPMl+oPEcHy3Vw/Mk9W6c8eBhF78GkNbtmijGKS0bbY/UX/fnugqgwE0Mmyrs0I11jERazwGpzn343rNl07G0eu4NMnyvhS0Fu/MKiuGWLm930zWOE";

        $service = new Services\TradingService([
            'siteId' => Constants\SiteIds::US,
            'sandbox' => env('SANDBOX'),
            'credentials' => [
                'appId'  => $appId,
                'certId' => $certId,
                'devId'  => $devId
            ]
        ]);

        $request = new Types\GetCategoriesRequestType();
        $request->RequesterCredentials = new Types\CustomSecurityHeaderType();
        $request->RequesterCredentials->eBayAuthToken = $eBayAuthToken;

        $request->DetailLevel = ['ReturnAll'];
        $request->CategoryParent = ['11116'];
        $request->LevelLimit = 99;

        $request->OutputSelector = [
            'CategoryArray.Category.CategoryID',
            'CategoryArray.Category.CategoryParentID',
            'CategoryArray.Category.CategoryLevel',
            'CategoryArray.Category.CategoryName'
        ];

        $res1 = $service->getCategories($request);

        printf("Category name [category ID]<br><br>");

        if ($res1->Ack !== 'Failure') {
            foreach ($res1->CategoryArray->Category as $category) {
                $lvl = '';
                if($category->CategoryLevel > 1)
                    $lvl = str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', $category->CategoryLevel - 1);

                printf(
                    "%s %s [%s]<br>",
                    $lvl,
                    //$category->CategoryLevel,
                    $category->CategoryName,
                    $category->CategoryID
                    //$category->CategoryParentID[0]
                );
            }
        }
    }

    public function getRedBookCategories()
    {
        $redBook = RedBookCategory::all();

        printf("Red Book Category name<br><br>");

        foreach ($redBook as $category) {
            $lvl = '';
            $categoryLevel = 1;
            //if($category->CategoryLevel > 1)
            //    $lvl = str_repeat('    ', $category->CategoryLevel - 1);

            if($category->parent_id) {
                $categoryLevel = $categoryLevel + 1;
                $qq = RedBookCategory::where('id', $category->parent_id)->first();
                if ($qq){
                    if ($qq->parent_id) {
                        $categoryLevel = $categoryLevel + 1;
                        $qq = RedBookCategory::where('id', $qq->parent_id)->first();
                        if ($qq){
                            if ($qq->parent_id) {
                                $categoryLevel = $categoryLevel + 1;
                                $qq = RedBookCategory::where('id', $qq->parent_id)->first();
                                if ($qq){
                                    if ($qq->parent_id) {
                                        $categoryLevel = $categoryLevel + 1;
                                        $qq = RedBookCategory::where('id', $qq->parent_id)->first();
                                        if ($qq){
                                            if ($qq->parent_id) {
                                                $categoryLevel = $categoryLevel + 1;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $lvl = str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', $categoryLevel - 1);
            printf(
                "%s %s <br>",
                //$category->parent_id,
                $lvl,
                $category->name
                //$category->id
            );
        }
    }

    public function GetCoinListingByDPN () {
        ini_set('default_socket_timeout', '1000');

        $options = array(
            'cache_wsdl'   => WSDL_CACHE_NONE,
            'trace'        => true,
            'exceptions'   => false,
            'soap_version' => SOAP_1_2,
        );

        $client = new \SoapClient('https://dealer.collectorscorner.com/WebServices/CCListings.asmx?wsdl', $options);

        $auth = [
            'Username' => '',
            'Password' => ''
        ];
        $header = new \SoapHeader('http://dealer.collectorscorner.com/webservices', 'AuthenticationSoapHeader', $auth, false);
        $client->__setSoapHeaders($header);

        $param = ['DealerProductNumber' => 64235353443];

        $result = $client->GetCoinListingByDPN($param);
        dd($result->GetCoinListingByDPNResult);

        $cont = new \SimpleXMLElement($result->GetCoinListingByDPNResult->ResponseDataSet->any);

        //dd($cont->NewDataSet->vwProductCoins);

        $json = json_encode($cont->NewDataSet->vwProductCoins);
        $content = json_decode($json);

        dd($content);
    }

    public function InsertCoinListingByPCGSCertNo () {
        ini_set('default_socket_timeout', '1000');

        $options = array(
            'cache_wsdl'   => WSDL_CACHE_NONE,
            'trace'        => true,
            'exceptions'   => false,
            'soap_version' => SOAP_1_2,
        );

        $client = new \SoapClient('https://dealer.collectorscorner.com/WebServices/CCListings.asmx?wsdl', $options);

        $auth = [
            'Username' => '',
            'Password' => ''
        ];
        $header = new \SoapHeader('http://dealer.collectorscorner.com/webservices', 'AuthenticationSoapHeader', $auth, false);
        $client->__setSoapHeaders($header);

        $param = [
            'CertNo' => '1215646',
            'DealerProductNumber' => 64235353443,
            'RetailPrice' => 13.00,
            'WholesalePrice' => 12.00,
            'ProductDescription' =>'string111',
            'ImageURL1' => '',
            'ImageURL2' => '',
        ];

        $result = $client->InsertCoinListingByPCGSCertNo($param);
        dd($result);

        //$cont = new \SimpleXMLElement($result->GetCoinListingByDPNResult->ResponseDataSet->any);

        //dd($cont->NewDataSet->vwProductCoins);

        // $json = json_encode($cont->NewDataSet->vwProductCoins);
        // $content = json_decode($json);

        // dd($content);
    }


    public function InsertCoinListingByCriteria () {
        ini_set('default_socket_timeout', '1000');

        $param = CoinCollector::sendSOAP(164534);
        dd($param);

        $options = array(
            'cache_wsdl'   => WSDL_CACHE_NONE,
            'trace'        => true,
            'exceptions'   => false,
            'soap_version' => SOAP_1_2,
        );

        $client = new \SoapClient('https://dealer.collectorscorner.com/WebServices/CCListings.asmx?wsdl', $options);

        $auth = [
            'Username' => '',
            'Password' => ''
        ];
        $header = new \SoapHeader('http://dealer.collectorscorner.com/webservices', 'AuthenticationSoapHeader', $auth, false);
        $client->__setSoapHeaders($header);

        $param = [
            'DealerProductNumber' => '',
            'RetailPrice' => 13.00,
            'WholesalePrice' => 13.00,
            'Quantity' => 1,
            'ProductTitle' => '1887 Morgan PCGS AU-58 VAM 12 Alligator Eye TOP 1001887 Morgan PCGS AU-58 VAM 12 Alligator Eye TOP 1001887 Morgan PCGS AU-58 VAM 12 Alligator Eye TOP 1001887 Morgan PCGS AU-58 VAM 12 Alligator Eye TOP 100',
            'ProductDescription' =>'Descript test',
            'ImageURL1' => '',
            'ImageURL2' => '',
            'GradingService' => 'ANACS',
            'Grade' => 2,
            'CategoryCode' => '20C',
            'YearIssued' => '1500',
            'Mint' => '',
            'Metal' => '',
            'MSPR' => '',
            'Attribute' => ''
        ];

        $result = $client->InsertCoinListingByCriteria($param);
        //dd($client);
        dd($result->InsertCoinListingByCriteriaResult);

        //$cont = new \SimpleXMLElement($result->GetCoinListingByDPNResult->ResponseDataSet->any);

        //dd($cont->NewDataSet->vwProductCoins);

        // $json = json_encode($cont->NewDataSet->vwProductCoins);
        // $content = json_decode($json);

        // dd($content);
    }


}
