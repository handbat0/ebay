<?php

namespace ppm;

use Illuminate\Database\Eloquent\Model;

class MarketplaceItemSetting extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'id',
        'product_id',
        'marketplace',
        'export',
        'pricing',
        'shipping',
        'shipping_cost',
        'shipping_additional_cost',
        'handling_time',
        'pickup',
        'pickup_cost',
        'pickup_additional_cost',
        'global_shipping',
    ];
}
