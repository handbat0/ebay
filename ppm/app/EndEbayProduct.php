<?php

namespace ppm;

use \DTS\eBaySDK\Constants;
use \DTS\eBaySDK\Trading\Services;
use \DTS\eBaySDK\Trading\Types;
use \DTS\eBaySDK\Trading\Enums;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class EndEbayProduct extends Model
{
    public static function EndProduct($ebay_id, $credentials = null)
    {
        // $credentials - for UnitTest
        if ($credentials) {
            $appId = $credentials["appId"];
            $certId = $credentials["certId"];
            $devId = $credentials["devId"];
            $eBayAuthToken = $credentials["eBayAuthToken"];
        } else {
            $ebay_keys = Marketplace::where('type', 'eBay')->first();

            if (!($ebay_keys && $ebay_keys->api_token)) {
                return ['error' => 'Not connect to eBay API. Check connection details.'];
            } else {
                $appId = $ebay_keys->app_id;
                $certId = $ebay_keys->cert_id;
                $devId = $ebay_keys->dev_id;
                $eBayAuthToken = $ebay_keys->api_token;
            }
        }

        $service = new Services\TradingService([
            'siteId' => Constants\SiteIds::US,
            'sandbox' => env('SANDBOX'),
            'credentials' => [
                'appId'  => $appId,
                'certId' => $certId,
                'devId'  => $devId
            ]
        ]);

        $request = new Types\EndItemRequestType();
        $request->RequesterCredentials = new Types\CustomSecurityHeaderType();
        $request->RequesterCredentials->eBayAuthToken = $eBayAuthToken;

        $request->ItemID = strval($ebay_id);
        $request->EndingReason = Enums\EndReasonCodeType::C_NOT_AVAILABLE;

        $response = $service->endItem($request);
        return $response;
    }
}