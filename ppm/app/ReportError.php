<?php

namespace ppm;

use Illuminate\Database\Eloquent\Model;

class ReportError extends Model
{
    protected $fillable = [
        'id',
        'error_code',
        'message',
        'count',
        'mp_type',
    ];
}
