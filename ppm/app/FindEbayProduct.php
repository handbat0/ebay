<?php

namespace ppm;

use \DTS\eBaySDK\Shopping\Services;
use \DTS\eBaySDK\Shopping\Types;
use \DTS\eBaySDK\Shopping\Enums;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use ppm\Marketplace;

class FindEbayProduct extends Model
{
    public static function FindProduct($id, $credentials = null)
    {
        // $credentials - for UnitTest
        if ($credentials) {
            $appId = $credentials["appId"];
            $certId = $credentials["certId"];
            $devId = $credentials["devId"];
        } else {
            $ebay_keys = Marketplace::where('type', 'eBay')->first();

            if (!$ebay_keys) {
                return ['error' => 'Not connect to eBay API. Check connection details.'];
            } else {
                $appId = $ebay_keys->app_id;
                $certId = $ebay_keys->cert_id;
                $devId = $ebay_keys->dev_id;
            }
        }

        $service = new Services\ShoppingService([
            'sandbox' => env('SANDBOX'),
            'credentials' => [
                'appId'  => $appId,
                'certId' => $certId,
                'devId'  => $devId
            ]
        ]);

        $request = new Types\GetSingleItemRequestType();
        $request->ItemID = $id;
        $request->IncludeSelector = 'Details';

        $response = $service->getSingleItem($request);

        return $response;
    }
}
