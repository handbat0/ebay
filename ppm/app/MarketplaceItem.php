<?php

namespace ppm;

use \DTS\eBaySDK\Constants;
use \DTS\eBaySDK\Trading\Services;
use \DTS\eBaySDK\Trading\Types;
use \DTS\eBaySDK\Trading\Enums;
use Illuminate\Support\Facades\Auth;

use Illuminate\Database\Eloquent\Model;

class MarketplaceItem extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'id',
        'product_id',
        'marketplace_id',
        'marketplace_type',
    ];
}
