<?php

namespace ppm;

use \DTS\eBaySDK\Constants;
use \DTS\eBaySDK\Trading\Services;
use \DTS\eBaySDK\Trading\Types;
use \DTS\eBaySDK\Trading\Enums;
use Illuminate\Support\Facades\Auth;
use ppm\Marketplace;

use Illuminate\Database\Eloquent\Model;


class AllEbayProducts extends Model
{
    public static function AllProducts($credentials = null)
    {
        // $credentials - for UnitTest
        if ($credentials) {
            $appId = $credentials["appId"];
            $certId = $credentials["certId"];
            $devId = $credentials["devId"];
            $eBayAuthToken = $credentials["eBayAuthToken"];
        } elseif (Auth::id()) {
            $ebay_keys = Marketplace::where('type', 'eBay')->first();

            if (!($ebay_keys && $ebay_keys->api_token)) {
                return 0;
            } else {
                $appId = $ebay_keys->app_id;
                $certId = $ebay_keys->cert_id;
                $devId = $ebay_keys->dev_id;
                $eBayAuthToken = $ebay_keys->api_token;
            }
        } else {
            return 0;
        }

        $service = new Services\TradingService([
            'siteId' => Constants\SiteIds::US,
            'sandbox' => env('SANDBOX'),
            'credentials' => [
                'appId'  => $appId,
                'certId' => $certId,
                'devId'  => $devId
            ]
        ]);

        $request = new Types\GetMyeBaySellingRequestType();
        $request->RequesterCredentials = new Types\CustomSecurityHeaderType();
        $request->RequesterCredentials->eBayAuthToken = $eBayAuthToken;

        $request->ActiveList = new Types\ItemListCustomizationType();
        $request->ActiveList->Include = true;

        $request->ActiveList->Sort = Enums\ItemSortTypeCodeType::C_CURRENT_PRICE_DESCENDING;

        $response = $service->getMyeBaySelling($request);

        return $response;
    }
}
