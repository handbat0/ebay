<?php

namespace ppm\Api\V1\Controllers;

use Dingo\Api\Http\Request;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Facades\Excel;
use ppm\Api\V1\Actions\Reports\DeliveriesReport;
use ppm\Api\V1\Actions\Reports\ProfitLossReport;
use ppm\Api\V1\Actions\Reports\PurchasesReport;
use ppm\Api\V1\Actions\Reports\SalesTaxReport;
use ppm\Api\V1\Actions\Reports\SpotPriceList;
use ppm\Api\V1\Reports\Deliveries;
use ppm\Api\V1\Reports\Purchases;
use ppm\Api\V1\Reports\SalesTax;
use ppm\Models\Products\Location;
use ppm\Models\Users\Employee;

use ppm\Api\V1\Actions\MarketplaceReports\ListMarketplaceReports;
use ppm\Api\V1\Transformers\MarketplaceReportTransformer;
use ppm\Report;
use ppm\CreateEbayProduct;
use ppm\UpdateEbayProductQuantity;
use ppm\UpdateEbayProduct;
use ppm\EndEbayProduct;
use ppm\Models\RedBookCategory;
use ppm\Models\Products\Product;
use ppm\Models\Products\Material;
use ppm\Models\Products\ProductImage;
use Illuminate\Database\Query\Builder;
use ppm\Marketplace;
use ppm\MarketplaceItem;
use Illuminate\Support\Facades\DB;
use ppm\Models\Marketplaces\eBay;
use ppm\Models\Marketplaces\CoinCollector;

class ReportsController extends ApiController
{
    public function marketplaceReports(Request $request, ListMarketplaceReports $action)
    {
        $params            = array_only($request->query(), ['sort', 'filters', 'items_per_page', 'page']);
        $params['filters'] = isset($params['filters']) ? $params['filters'] : [];

        return $this->response->paginator($action->execute($params, $this->getUser()), new MarketplaceReportTransformer());
    }

    public function callRepeatMarketplaceReports(Request $request)
    {
        $report_id = $request->report_id;

        if (!empty($report_id)) {
            $report = Report::find($report_id);
            if ($report) {
                $this->repeatMarketplaceReports($report);
            }
        }
        return ['data' => 'success'];
    }

    public function autoRepeatMarketplaceReports($report_id)
    {
        if (!empty($report_id)) {
            $report = Report::find($report_id);
            if ($report) {
                $this->repeatMarketplaceReports($report);
            }
        }
    }

    private function repeatMarketplaceReports(Report $report)
    {
        $retries = 50;
        $item = Product::find($report->product_id);

        switch ($report->marketplace_type) {
            case 'eBay';
                switch ($report->type) {
                    case 'create';
                        $status = eBay::itemCreate($item);
                        break;
                    case 'update';
                        $status = eBay::itemUpdate($item);
                        break;
                    case 'delete';
                        $status = eBay::itemDelete($item);
                        if ($status == 'Success') {
                            return true;
                        }
                        break;
                    default;
                        break;
                }
                break;
            case 'CoinCollector';
                switch ($report->type) {
                    case 'create';
                        $status = CoinCollector::itemCreate($item);
                        break;
                    case 'update';
                        $status = CoinCollector::itemUpdate($item);
                        break;
                    case 'delete';
                        $status = CoinCollector::itemDelete($item);
                        if ($status == 'Success') {
                            return true;
                        }
                        break;
                    default;
                        break;
                }
                break;
            default;
                break;
        }

        $report->refresh();
        if ($report->retries >= $retries && $report->status == 'in_progress') {
            $report->status = 'error';
            $report->retries = 0;
            $report->save();
        }
        return true;
    }

    public function deleteMarketplaceReports(Request $request)
    {
        $report_id = $request->query('report_id');
        if (!empty($report_id)) {
            $report = Report::find($report_id);
            $report->delete();
        }
        return ['data' => 'success'];
    }
}
