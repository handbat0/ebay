<?php

namespace ppm\Api\V1\Controllers\Settings;

use ppm\Marketplace;
use Dingo\Api\Http\Request;
use ppm\Api\V1\Controllers\ApiController;
use Illuminate\Support\Facades\Auth;
use ppm\MarketplaceItemSetting;
use ppm\Models\Marketplaces\eBay;

class MarketplaceController extends ApiController
{
    public function index()
    {
        $global_settings = Marketplace::all();

        if($global_settings) {
            return $global_settings;
        }

        return 0;
    }

    public function updateKeys(Request $request)
    {
        if ($request["type"] == 'eBay') {
            $credentials = [
                'appId' => $request["app_id"],
                'certId' => $request["cert_id"],
                'devId' => $request["dev_id"],
                'eBayAuthToken' => $request["api_token"]
            ];
            $connectEbayApi = eBay::getEbayTime($credentials);
        } elseif ($request["type"] == 'CoinCollector') {
            if ($request["u_name"] && $request["u_pwd"]) {
                $connectEbayApi = true;
            } else {
                $connectEbayApi = false;
            }
        }else {
            return 'Error. Undefine marketplace '. $request["type"] .'.';
        }

        if (!$connectEbayApi) {
            return 'Error. Not connect to '. $request["type"] .' API. Check connection details.';
        }

        if ($request["type"] == 'eBay') {
            if (!filter_var($request["paypal_email"], FILTER_VALIDATE_EMAIL)) {
                return 'Error. Invalid email address: '. $request["paypal_email"] .'. This field is required for exporting goods..';
            }
        }

        Marketplace::updateOrCreate(['type' => $request["type"]],[
            'api_token' => $request["api_token"],
            'app_id' => $request["app_id"],
            'cert_id' => $request["cert_id"],
            'dev_id' => $request["dev_id"],

            'paypal_email' => $request["paypal_email"],

            'u_name' => $request["u_name"],
            'u_pwd' => $request["u_pwd"],

            'bullion' => $request["categories"]["bullion"] ? 1 : 0,
            'scrap' => $request["categories"]["scrap"] ? 1 : 0,
            'gems' => $request["categories"]["gems"] ? 1 : 0,
            'inum' => $request["categories"]["inum"] ? 1 : 0,
            'bnum' => $request["categories"]["bnum"] ? 1 : 0,

            'bullion_sh' => $request["shipping"]["bullion"] ? $request["shipping"]["bullion"] : "",
            'scrap_sh' => $request["shipping"]["scrap"] ? $request["shipping"]["scrap"] : "",
            'gems_sh' => $request["shipping"]["gems"] ? $request["shipping"]["gems"] : "",
            'inum_sh' => $request["shipping"]["inum"] ? $request["shipping"]["inum"] : "",
            'bnum_sh' => $request["shipping"]["bnum"] ? $request["shipping"]["bnum"] : "",

            'bullion_pr' => $request["pricing"]["bullion"] ? $request["pricing"]["bullion"] : 0,
            'scrap_pr' => $request["pricing"]["scrap"] ? $request["pricing"]["scrap"] : 0,
            'gems_pr' => $request["pricing"]["gems"] ? $request["pricing"]["gems"] : 0,
            'inum_pr' => $request["pricing"]["inum"] ? $request["pricing"]["inum"] : 0,
            'bnum_pr' => $request["pricing"]["bnum"] ? $request["pricing"]["bnum"] : 0,

            'bullion_sh_cost' => $request["cost"]["bullion"] ? $request["cost"]["bullion"] : 0,
            'scrap_sh_cost' => $request["cost"]["scrap"] ? $request["cost"]["scrap"] : 0,
            'gems_sh_cost' => $request["cost"]["gems"] ? $request["cost"]["gems"] : 0,
            'inum_sh_cost' => $request["cost"]["inum"] ? $request["cost"]["inum"] : 0,
            'bnum_sh_cost' => $request["cost"]["bnum"] ? $request["cost"]["bnum"] : 0,

            'bullion_sh_add_cost' => $request["add_cost"]["bullion"] ? $request["add_cost"]["bullion"] : 0,
            'scrap_sh_add_cost' => $request["add_cost"]["scrap"] ? $request["add_cost"]["scrap"] : 0,
            'gems_sh_add_cost' => $request["add_cost"]["gems"] ? $request["add_cost"]["gems"] : 0,
            'inum_sh_add_cost' => $request["add_cost"]["inum"] ? $request["add_cost"]["inum"] : 0,
            'bnum_sh_add_cost' => $request["add_cost"]["bnum"] ? $request["add_cost"]["bnum"] : 0,

            'bullion_handling_time' => $request["handling_time"]["bullion"] ? $request["handling_time"]["bullion"] : 0,
            'scrap_handling_time' => $request["handling_time"]["scrap"] ? $request["handling_time"]["scrap"] : 0,
            'gems_handling_time' => $request["handling_time"]["gems"] ? $request["handling_time"]["gems"] : 0,
            'inum_handling_time' => $request["handling_time"]["inum"] ? $request["handling_time"]["inum"] : 0,
            'bnum_handling_time' => $request["handling_time"]["bnum"] ? $request["handling_time"]["bnum"] : 0,

            'bullion_pickup' => $request["pickup"]["bullion"] ? $request["pickup"]["bullion"] : 0,
            'scrap_pickup' => $request["pickup"]["scrap"] ? $request["pickup"]["scrap"] : 0,
            'gems_pickup' => $request["pickup"]["gems"] ? $request["pickup"]["gems"] : 0,
            'inum_pickup' => $request["pickup"]["inum"] ? $request["pickup"]["inum"] : 0,
            'bnum_pickup' => $request["pickup"]["bnum"] ? $request["pickup"]["bnum"] : 0,

            'bullion_pickup_cost' => $request["pickup_cost"]["bullion"] ? $request["pickup_cost"]["bullion"] : 0,
            'scrap_pickup_cost' => $request["pickup_cost"]["scrap"] ? $request["pickup_cost"]["scrap"] : 0,
            'gems_pickup_cost' => $request["pickup_cost"]["gems"] ? $request["pickup_cost"]["gems"] : 0,
            'inum_pickup_cost' => $request["pickup_cost"]["inum"] ? $request["pickup_cost"]["inum"] : 0,
            'bnum_pickup_cost' => $request["pickup_cost"]["bnum"] ? $request["pickup_cost"]["bnum"] : 0,

            'bullion_pickup_additional_cost' => $request["pickup_additional_cost"]["bullion"] ? $request["pickup_additional_cost"]["bullion"] : 0,
            'scrap_pickup_additional_cost' => $request["pickup_additional_cost"]["scrap"] ? $request["pickup_additional_cost"]["scrap"] : 0,
            'gems_pickup_additional_cost' => $request["pickup_additional_cost"]["gems"] ? $request["pickup_additional_cost"]["gems"] : 0,
            'inum_pickup_additional_cost' => $request["pickup_additional_cost"]["inum"] ? $request["pickup_additional_cost"]["inum"] : 0,
            'bnum_pickup_additional_cost' => $request["pickup_additional_cost"]["bnum"] ? $request["pickup_additional_cost"]["bnum"] : 0,
        ]);

        return 'success';
    }

    public function getMarketplace(Request $request)
    {
        $data = $request->all();
        // get product_id (if 0 - create)
        $id = $data['product_id'];
        $type = $data['coin_type'];
        $item_marketplaces = [];

        $global_settings = Marketplace::all();

        // dont't show form if not marketplace
        if (!$global_settings) {
            return ['nothing_export' => true];
        }

        $count = count($global_settings);
        for ($i = 0; $i < $count; $i++) {
            $connectEbayApi = false;
            if ($global_settings[$i]->type == 'eBay') {
                // show error if not dont' connect to marketplace
                $credentials = [
                    'appId' => $global_settings[$i]->app_id,
                    'certId' => $global_settings[$i]->cert_id,
                    'devId' => $global_settings[$i]->dev_id,
                    'eBayAuthToken' => $global_settings[$i]->api_token
                ];
                $connectEbayApi = eBay::getEbayTime($credentials);
            } elseif ($global_settings[$i]->type == 'CoinCollector') {
                if ($global_settings[$i]->u_name && $global_settings[$i]->u_pwd) {
                    $connectEbayApi = true;
                }
            }

            if (!$connectEbayApi) {
                return ['error' => 'Not connect to ' . $global_settings[$i]->type . ' API. Check connection details'];
            }

            // default settings
            if ($type == 'bulk') {
                if ((bool)$global_settings[$i]->bnum) {
                    $item_marketplaces[$global_settings[$i]->type] = [
                        'status' => (bool)$global_settings[$i]->bnum,
                        'shipping' => $global_settings[$i]->bnum_sh,
                        'export' => true,
                        'pricing' => $global_settings[$i]->bnum_pr,
                        'shipping_cost' => $global_settings[$i]->bnum_sh_cost,
                        'shipping_additional_cost' => $global_settings[$i]->bnum_sh_add_cost,
                        'handling_time' => $global_settings[$i]->bnum_handling_time,
                        'pickup' => $global_settings[$i]->bnum_pickup,
                        'pickup_cost' => $global_settings[$i]->bnum_pickup_cost,
                        'pickup_additional_cost' => $global_settings[$i]->bnum_pickup_additional_cost,
                    ];

                    if ($global_settings[$i]->type == 'eBay') {
                        $item_marketplaces[$global_settings[$i]->type]['global_shipping'] = true;
                    }
                }
            } elseif ($type == 'individual') {
                if ((bool)$global_settings[$i]->inum) {
                    $item_marketplaces[$global_settings[$i]->type] = [
                        'status' => (bool)$global_settings[$i]->inum,
                        'shipping' => $global_settings[$i]->inum_sh,
                        'export' => true,
                        'pricing' => $global_settings[$i]->inum_pr,
                        'shipping_cost' => $global_settings[$i]->inum_sh_cost,
                        'shipping_additional_cost' => $global_settings[$i]->inum_sh_add_cost,
                        'handling_time' => $global_settings[$i]->inum_handling_time,
                        'pickup' => $global_settings[$i]->inum_pickup,
                        'pickup_cost' => $global_settings[$i]->inum_pickup_cost,
                        'pickup_additional_cost' => $global_settings[$i]->inum_pickup_additional_cost,
                    ];

                    if ($global_settings[$i]->type == 'eBay') {
                        $item_marketplaces[$global_settings[$i]->type]['global_shipping'] = true;
                    }
                }
            }
        }

        //dont't show form if unchecked export in settings
        if (!count($item_marketplaces)) {
            return ['nothing_export' => true];
        }

        // if edit coin
        if ($id) {
            $marketplaces = MarketplaceItemSetting::where('product_id', $id)->get();

            if (count($marketplaces)) {
                foreach ($marketplaces as $marketplace) {
                    $shop = $marketplace->marketplace;
                    if (isset($item_marketplaces[$shop])) {
                        if ($item_marketplaces[$shop]['status']) {
                            $item_marketplaces[$shop] = [
                                'status' => true,
                                'shipping' => $marketplace->shipping,
                                'export' => (bool)$marketplace->export,
                                'pricing' => $marketplace->pricing,
                                'shipping_cost' => $marketplace->shipping_cost,
                                'shipping_additional_cost' => $marketplace->shipping_additional_cost,
                                'handling_time' => $marketplace->handling_time,
                                'pickup' => $marketplace->pickup,
                                'pickup_cost' => $marketplace->pickup_cost,
                                'pickup_additional_cost' => $marketplace->pickup_additional_cost,
                                'global_shipping' => $marketplace->global_shipping,
                            ];
                        } else {
                            unset($item_marketplaces[$shop]);
                        }
                    }
                }
            }
        }

        //dont't show form if unchecked export in settings
        if (!count($item_marketplaces)) {
            return ['nothing_export' => true];
        }

        return $item_marketplaces;
    }
}
