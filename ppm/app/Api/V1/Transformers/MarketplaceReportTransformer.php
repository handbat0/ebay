<?php

namespace ppm\Api\V1\Transformers;

use Carbon\Carbon;

use Illuminate\Database\Eloquent\Collection;
use League\Fractal\TransformerAbstract;
use ppm\Report;

class MarketplaceReportTransformer extends TransformerAbstract
{
    public function transform(Report $report)
    {
        $data = array_only($report->toArray(), [
            'id',
            'product_id',
            'product_type',
            'description',
            'type',
            'status',
            'message',
            'data',
            'marketplace_type',
            'name',
            'quantity',
            'expiration_date',
            'link',
            'retries',
            'created_at',
            'updated_at',
        ]);

        return $data;
    }
}
