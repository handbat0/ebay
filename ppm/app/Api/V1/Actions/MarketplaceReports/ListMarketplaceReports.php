<?php

namespace ppm\Api\V1\Actions\MarketplaceReports;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Validation\Factory as ValidatorFactory;
use ppm\Api\Exceptions\InvalidDataException;
use ppm\Api\V1\Actions\AbstractAction;
use ppm\Report;
use ppm\Models\User;
use ppm\Models\Users\Employee;

class ListMarketplaceReports extends AbstractAction
{

    /**
     * @var Report
     */
    protected $report;

    /**
     * @var ValidatorFactory
     */
    protected $validatorFactory;

    public function __construct(Report $report, ValidatorFactory $validationFactory)
    {
        $this->report           = $report;
        $this->validatorFactory = $validationFactory;
    }

    protected function getValidationRules()
    {
        $rules = [
            'product_id'          => 'nullable|exists:product_id',
            'type'                => 'nullable|exists:type',
            'status'              => 'nullable|exists:status',
            'message'             => 'nullable|exists:message',
            'data'                => 'nullable|exists:data',
            'marketplace_type'    => 'nullable|exists:marketplace_type',
            'name'                => 'nullable|exists:name',
            'quantity'            => 'nullable|exists:quantity',
            'expiration_date'     => 'nullable|exists:expiration_date',
            'link'                => 'nullable|exists:link',
            'retries'             => 'nullable|exists:retries',
        ];

        return $rules;
    }

    public function validate($data)
    {
        $validator = $this->validatorFactory->make($data, $this->getValidationRules());

        if ($validator->fails()) {
            throw new InvalidDataException('Data provided is not valid', $validator->errors());
        }
    }

    /**
     * @param array $parameters
     * @param User $caller
     *
     * @return mixed
     */
    public function execute($parameters, User $caller = null)
    {
        $this->ensureCaller($caller);

        $this->validate($parameters);

        $employeeIds = Employee::excludeSuperadmins()->get()->pluck('id');

        $query = $this->report->newQuery()
                                ->join('products', 'products.id', '=', 'reports.product_id')
                                ->select('reports.*', 'products.entry_type as product_type', 'products.description');

        if (isset($parameters['filters'])) {
            $query = $this->applySorting($query, array_get($parameters, 'sort'));
            $query = $this->applyFilters($query, array_get($parameters, 'filters'));

            return $query->paginate(array_get($parameters, 'items_per_page', 10));
        } else {
            if (array_get($parameters, 'product_id')) {
                $query = $query->forProductId(array_get($parameters, 'product_id'));
            }

            // if (array_get($parameters, 'product_type')) {
            //     $query = $query->forForProductType(array_get($parameters, 'product_type'));
            // }

            if (array_get($parameters, 'product_type')) {
                $query = $query->forForProductEntryType(array_get($parameters, 'product_type'));
            }

            if (array_get($parameters, 'type')) {
                $query = $query->forType(array_get($parameters, 'type'));
            }

            if (array_get($parameters, 'status')) {
                $query = $query->forStatus(array_get($parameters, 'status'));
            }

            if (array_get($parameters, 'marketplace_type')) {
                $query = $query->forMarketplaceType(array_get($parameters, 'marketplace_type'));
            }

            if (array_get($parameters, 'name')) {
                $query = $query->forName(array_get($parameters, 'name'));
            }

            if (array_get($parameters, 'quantity')) {
                $query = $query->forQuantity(array_get($parameters, 'quantity'));
            }

            if (array_get($parameters, 'expiration_date')) {
                $query = $query->forExpirationDate(array_get($parameters, 'expiration_date'));
            }

            if (array_get($parameters, 'retries')) {
                $query = $query->forRetries(array_get($parameters, 'retries'));
            }
        }

        return $query->get();
    }


    protected function applySorting(Builder $query, $sort)
    {
        $field     = array_get($sort, 'field');
        $direction = array_get($sort, 'dir', 'asc');

        if ($field) {
            // if ($field == 'user.name') {
            //     $field = 'users.first_name';
            //     $query = $query->join('users', 'audit_trails.user_id', '=', 'users.id');
            // }

            return $query->orderBy($field, $direction);
        }

        return $query;
    }

    protected function applyFilters(Builder $query, $filters)
    {
        if ( ! is_array($filters)) {
            return $query;
        }

        foreach ($filters as $field => $value) {
            if ($value === null) {
                continue;
            }
            switch ($field) {
                case 'end_date';
                    $query->where('reports.created_at', '<=', Carbon::parse($value)->endOfDay());
                    break;
                case 'start_date';
                    $query->where('reports.created_at', '>=', Carbon::parse($value)->startOfDay());
                    break;
                case 'product_type';
                    $entry_type ='bulk';
                    if ($value == 'inum') {
                        $entry_type = 'individual';
                    }
                    $query->where('products.entry_type', $entry_type);
                    break;
                default:
                    $query = $query->where('reports.' . $field, $value);
                    break;
            }
        }

        return $query;
    }
}
