<?php

namespace ppm;

use \DTS\eBaySDK\Constants;
use \DTS\eBaySDK\Trading\Services;
use \DTS\eBaySDK\Trading\Types;
use \DTS\eBaySDK\Trading\Enums;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class UpdateEbayProduct extends Model
{
    public static function UpdateProduct($params)
    {
        // $params["credentials"] - for UnitTest
        if (isset($params["credentials"])) {
            $appId = $params["credentials"]["appId"];
            $certId = $params["credentials"]["certId"];
            $devId = $params["credentials"]["devId"];
            $eBayAuthToken = $params["credentials"]["eBayAuthToken"];
        } else {
            $ebay_keys = Marketplace::where('type', 'eBay')->first();
            if (!($ebay_keys && $ebay_keys->api_token)) {
                return ['error' => 'Not connect to eBay API. Check connection details.'];
            } else {
                $appId = $ebay_keys->app_id;
                $certId = $ebay_keys->cert_id;
                $devId = $ebay_keys->dev_id;
                $eBayAuthToken = $ebay_keys->api_token;
            }
        }

        $service = new Services\TradingService([
            'siteId' => Constants\SiteIds::US,
            'sandbox' => env('SANDBOX'),
            'credentials' => [
                'appId'  => $appId,
                'certId' => $certId,
                'devId'  => $devId
            ]
        ]);

        $request = new Types\ReviseFixedPriceItemRequestType();
        $request->RequesterCredentials = new Types\CustomSecurityHeaderType();
        $request->RequesterCredentials->eBayAuthToken = $eBayAuthToken;

        if (!isset($params["credentials"])) {
            // ignore for unit test
            $ebay = CreateEbayProduct::getEbayMarketplaceState($params["product_id"], $params['entry_type'], $ebay_keys);
            if (!$ebay) {
                return ['error' => 'Export of this item is disabled. Check product export settings and global export settings.'];
            }
        } else {
            // for UnitTest
            $ebay['export'] = true;
            $ebay['pricing'] = 10;
            $ebay['shipping_cost'] = 10;
            $ebay['shipping_additional_cost'] = 1;
            $ebay['handling_time'] = 30;
            $ebay['shipping'] = 'USPSFirstClass';
            $ebay['pickup'] = true;
            $ebay['pickup_cost'] = 1.1;
            $ebay['pickup_additional_cost'] = 0.5;
            $ebay['global_shipping'] = true;
        }

        $item = new Types\ItemType();
        $item->ItemID = strval($params["ebay_id"]);

        $item->Title = substr($params['title'], 0, 80);

        $item->PrimaryCategory = new Types\CategoryType();
        $item->PrimaryCategory->CategoryID = $params['category_id'];

        if (isset($params['picture'])) {
            $item->PictureDetails = new Types\PictureDetailsType();
            $item->PictureDetails->GalleryType = Enums\GalleryTypeCodeType::C_GALLERY;
            $pictures_array = [];
            for ($pic_index = 0; $pic_index < count($params['picture']); $pic_index++) {
                array_push($pictures_array, $params['picture'][$pic_index]);
            }
            $item->PictureDetails->PictureURL = $pictures_array;
        }

        $item->ItemSpecifics = new Types\NameValueListArrayType();
        if (!empty($params["certification"])) {
            $item->ItemSpecifics->NameValueList[] = new Types\NameValueListType([
                'Name' => 'Certification',
                'Value' => [$params["certification"]]
            ]);
        }
        if (!empty($params["composition"])) {
            $item->ItemSpecifics->NameValueList[] = new Types\NameValueListType([
                'Name' => 'Composition',
                'Value' => [$params["composition"]]
            ]);
        }
        if (!empty($params["grade"])) {
            $item->ItemSpecifics->NameValueList[] = new Types\NameValueListType([
                'Name' => 'Grade',
                'Value' => [$params["grade"]]
            ]);
        }
        if (!empty($params["year"])) {
            $item->ItemSpecifics->NameValueList[] = new Types\NameValueListType([
                'Name' => 'Year',
                'Value' => [$params["year"]]
            ]);
        }
        // Circulated/Uncirculated params always (bulk, individual)
        $item->ItemSpecifics->NameValueList[] = new Types\NameValueListType([
            'Name' => 'Circulated/Uncirculated',
            'Value' => ["Unknown"]
        ]);
        if (!empty($params["strike_type"])) {
            $item->ItemSpecifics->NameValueList[] = new Types\NameValueListType([
                'Name' => 'Strike Type',
                'Value' => [$params["strike_type"]]
            ]);
        }
        if (!empty($params["mint_location"])) {
            $item->ItemSpecifics->NameValueList[] = new Types\NameValueListType([
                'Name' => 'Mint Location',
                'Value' => [$params["mint_location"]]
            ]);
        }
        if (!empty($params["country"])) {
            $item->ItemSpecifics->NameValueList[] = new Types\NameValueListType([
                'Name' => 'Country',
                'Value' => [$params["country"]]
            ]);
        }

        $item->Description = $params['description'];//html

        $item->Quantity = (int)$params['quantity'];

        $price = number_format((double)$params['price'] * (1 + (double)$ebay['pricing'] / 100), 2, '.', '');
        $item->StartPrice = new Types\AmountType(['value' => (double)$price]);

        $item = CreateEbayProduct::createShipping($item, $price, $ebay, $params);

        $item->DispatchTimeMax = $ebay['handling_time'];

        $request->Item = $item;

        $response = $service->reviseFixedPriceItem($request);
        return $response; //for testing
    }
}
