<?php

namespace ppm;

use Illuminate\Database\Eloquent\Model;
use ppm\Models\Marketplaces\eBay;
use ppm\Models\Marketplaces\CoinCollector;
use ppm\Models\Products\Product;

class Marketplace extends Model
{
    protected $table = 'external_marketplaces';

    protected $fillable = [
        'type',
        'api_token',
        'app_id',
        'cert_id',
        'dev_id',

        'paypal_email',

        // for Coin Collector field
        'u_name',
        'u_pwd',

        'bullion',
        'scrap',
        'gems',
        'inum',
        'bnum',

        'bullion_sh',
        'scrap_sh',
        'gems_sh',
        'inum_sh',
        'bnum_sh',

        'bullion_pr',
        'scrap_pr',
        'gems_pr',
        'inum_pr',
        'bnum_pr',

        'bullion_sh_cost',
        'scrap_sh_cost',
        'gems_sh_cost',
        'inum_sh_cost',
        'bnum_sh_cost',

        'bullion_sh_add_cost',
        'scrap_sh_add_cost',
        'gems_sh_add_cost',
        'inum_sh_add_cost',
        'bnum_sh_add_cost',

        'bullion_handling_time',
        'scrap_handling_time',
        'gems_handling_time',
        'inum_handling_time',
        'bnum_handling_time',

        'bullion_pickup',
        'scrap_pickup',
        'gems_pickup',
        'inum_pickup',
        'bnum_pickup',

        'bullion_pickup_cost',
        'scrap_pickup_cost',
        'gems_pickup_cost',
        'inum_pickup_cost',
        'bnum_pickup_cost',

        'bullion_pickup_additional_cost',
        'scrap_pickup_additional_cost',
        'gems_pickup_additional_cost',
        'inum_pickup_additional_cost',
        'bnum_pickup_additional_cost',
    ];

    public static function individualTitle($year, $mint = '', $parent_name = '', $gradingService = '', $grade = '', $variety = '', $designation = '', $crop = true)
    {
        $title = $year . ' ' . $mint . ' ' . $parent_name . ' ' . $gradingService . ' ' . $grade . ' ' . $variety . ' ' . $designation;
        // Strip whitespace
        $title = preg_replace('/\s\s+/', ' ', $title);
        if ($crop) {
            $title = substr($title, 0, 80);
        }
        return $title;
    }

    public static function saveOrUpdateMarketplaceState($data, $product_id)
    {
        $marketplace_settings = MarketplaceItemSetting::where('product_id', $product_id)->get();
        if (count($marketplace_settings)) {
            foreach ($marketplace_settings as $marketplace) {
                $shop = $marketplace->marketplace;
                if (isset($data['selected_marketplaces'][$shop])) {
                    $marketplace->pricing = $data['selected_marketplaces'][$shop]['pricing'];
                    $marketplace->export = $data['selected_marketplaces'][$shop]['export'];
                    $marketplace->shipping = $data['selected_marketplaces'][$shop]['shipping'];
                    $marketplace->shipping_cost = $data['selected_marketplaces'][$shop]['shipping_cost'];
                    $marketplace->shipping_additional_cost = $data['selected_marketplaces'][$shop]['shipping_additional_cost'];
                    $marketplace->handling_time = $data['selected_marketplaces'][$shop]['handling_time'];
                    $marketplace->pickup = $data['selected_marketplaces'][$shop]['pickup'];
                    $marketplace->pickup_cost = $data['selected_marketplaces'][$shop]['pickup_cost'];
                    $marketplace->pickup_additional_cost = $data['selected_marketplaces'][$shop]['pickup_additional_cost'];
                    $marketplace->global_shipping = $data['selected_marketplaces'][$shop]['global_shipping'];
                    $marketplace->save();
                }
                unset($data['selected_marketplaces'][$shop]);
            }
        }
        if (isset($data['selected_marketplaces'])) {
            if (count($data['selected_marketplaces'])) {
                foreach ($data['selected_marketplaces'] as $key => $value) {
                    $marketplace_settings = MarketplaceItemSetting::create([
                        'product_id' => $product_id,
                        'marketplace' => $key,
                        'export' => $value['export'],
                        'pricing' => $value['pricing'],
                        'shipping' => $value['shipping'],
                        'shipping_cost' => $value['shipping_cost'],
                        'shipping_additional_cost' => $value['shipping_additional_cost'],
                        'handling_time' => $value['handling_time'],
                        'pickup' => $value['pickup'],
                        'pickup_cost' => $value['pickup_cost'],
                        'pickup_additional_cost' => $value['pickup_additional_cost'],
                        'global_shipping' => $value['global_shipping'],
                    ]);
                    if (!$marketplace_settings) {
                        return 'Settings for ' . $key . ' were not saved' . PHP_EOL;
                    }
                }
            }
        }
        return 'Success';
    }

    // create or update quantity (for bulk)
    public static function itemCreate($item_id)
    {
        $item = Product::find($item_id);
        if (!$item) {
            return 'Success';
        }

        $global_settings = Marketplace::all();

        $export_item = 'Success';
        foreach ($global_settings as $global_setting) {
            if (!($item->type == 'numismatic' && $item->sell_price_method == 'Fixed' && $item->sell_premium > 0)) {
                continue;
            }

            $settings = MarketplaceItemSetting::where([
                            'product_id' => $item_id,
                            'marketplace' => $global_setting->type
                        ])->first();

            if (!$settings) {
                continue;
            }

            if (!(bool)$settings->export) {
                continue;
            }

            $mp_item = MarketplaceItem::where([
                                    'product_id' => $item_id,
                                    'marketplace_type' => $global_setting->type
                                ])->first();

            if ($item->entry_type == 'bulk' && (bool)$global_setting->bnum) {
                if($mp_item) {
                    if ($item->total_stock <= 0) {
                        // ----------- MP action delete ---------- //
                        switch ($global_setting->type) {
                            case 'eBay':
                                $status = eBay::itemDelete($item);
                                break;
                            case 'CoinCollector':
                                $status = CoinCollector::itemDelete($item);
                                break;
                            default:
                                $status = 'Success';
                                break;
                        }
                        if ($status !== 'Success') {
                            $export_item = 'Error';
                        }
                        // ----------- end MP action delete ---------- //
                        continue;
                    }

                    // ----------- MP action update ---------- //
                    switch ($global_setting->type) {
                        case 'eBay':
                            $status = eBay::itemUpdate($item);
                            break;
                        case 'CoinCollector':
                            $status = CoinCollector::itemUpdate($item);
                            break;
                        default:
                            $status = 'Success';
                            break;
                    }
                    if ($status !== 'Success') {
                        $export_item = 'Error';
                    }
                    // ----------- end MP action update ---------- //
                    continue;
                } else {
                    if ($item->total_stock <= 0) {
                        continue;
                    }
                }
            } elseif ($item->entry_type == 'individual' && (bool)$global_setting->inum) {
                if($mp_item) {
                    continue;
                }
            } else {
                continue;
            }

            // ----------- MP action create ---------- //
            switch ($global_setting->type) {
                case 'eBay':
                    $status = eBay::itemCreate($item);
                    break;
                case 'CoinCollector':
                    $status = CoinCollector::itemCreate($item);
                    break;
                default:
                    $status = 'Success';
                    break;
            }
            if ($status !== 'Success') {
                $export_item = 'Error';
            }
            // ----------- end MP action create ---------- //
        }

        return $export_item;
    }

    // create or update quantity (for bulk)
    public static function itemUpdate($item_id)
    {
        $item = Product::find($item_id);
        if (!$item) {
            return 'Success';
        }

        $global_settings = Marketplace::all();

        $export_item = 'Success';
        foreach ($global_settings as $global_setting) {
            $new_mp_item = false;

            if ($item->type !== "numismatic") {
                continue;
            }

            $mp_item = MarketplaceItem::where([
                            'product_id' => $item_id,
                            'marketplace_type' => $global_setting->type
                        ])->first();

            if (!$mp_item) {
                if ($item->sell_premium <= 0 || $item->sell_price_method !== "Fixed") {
                    continue;
                }
                $new_mp_item = true;
            } else {
                // ----------- MP action delete ---------- //
                switch ($global_setting->type) {
                    case 'eBay':
                        $find_ebay = FindEbayProduct::FindProduct((string)$mp_item->marketplace_id);
                        if ($find_ebay->Ack == "Success") {
                            if ($find_ebay->Item->ListingStatus == "Ended") {
                                $mp_item->delete();
                                $new_mp_item = true;
                                $status = 'Success';
                            }
                            if ($item->total_stock <= 0 ||
                                $item->sell_premium <= 0 ||
                                $item->sell_price_method !== "Fixed"
                            ) {
                                $status = eBay::itemDelete($item);
                            }
                        } else {
                            $mp_item->delete();
                            $new_mp_item = true;
                            $status = 'Success';
                        }
                        break;
                    case 'CoinCollector':
                        $find_CC = CoinCollector::findItem($item);
                        if ($find_CC) {
                            if ($item->total_stock <= 0 ||
                                $item->sell_premium <= 0 ||
                                $item->sell_price_method !== "Fixed"
                            ) {
                                $status = CoinCollector::itemDelete($item);
                            }
                        } else {
                            $mp_item->delete();
                            $new_mp_item = true;
                            $status = 'Success';
                        }
                        break;
                    default:
                        $status = 'Success';
                        break;
                }
                if (!empty($status)) {
                    if ($status !== 'Success') {
                        $export_item = 'Error';
                    }
                }
                // ----------- end MP action delete ---------- //
            }

            $settings = MarketplaceItemSetting::where([
                            'product_id' => $item_id,
                            'marketplace' => $global_setting->type
                        ])->first();

            if (!$settings) {
                continue;
            }

            if (!(bool)$settings->export) {
                if ($mp_item) {
                    // ----------- MP action delete ---------- //
                    switch ($global_setting->type) {
                        case 'eBay':
                            $status = eBay::itemDelete($item);
                            break;
                        case 'CoinCollector':
                            $status = CoinCollector::itemDelete($item);
                            break;
                        default:
                            $status = 'Success';
                            break;
                    }
                    if ($status !== 'Success') {
                        $export_item = 'Error';
                    }
                    // ----------- end MP action delete ---------- //
                    continue;
                } else {
                    $report = Report::where(['product_id' => $item_id, 'marketplace_type' => $global_setting->type])->first();
                    if ($report) {
                        $report->delete();
                    }
                }
                continue;
            }

            if ($item->total_stock <= 0) {
                continue;
            }

            if ($item->entry_type == "bulk" && (bool)$global_setting->bnum) {
                $export = true;
            } elseif ($item->entry_type == "individual" && (bool)$global_setting->inum) {
                $export = true;
            } else {
                $export = false;
            }

            if ($export) {
                // ----------- MP action create update ---------- //
                switch ($global_setting->type) {
                    case 'eBay':
                        if ($new_mp_item) {
                            $status = eBay::itemCreate($item);
                        } else {
                            $status = eBay::itemUpdate($item);
                        }
                        break;
                    case 'CoinCollector':
                        if ($new_mp_item) {
                            $status = CoinCollector::itemCreate($item);
                        } else {
                            $status = CoinCollector::itemUpdate($item);
                        }
                        break;
                    default:
                        $status = 'Success';
                        break;
                }
                if ($status !== 'Success') {
                    $export_item = 'Error';
                }
                // ----------- end MP action create update ---------- //
            }
        }

        return $export_item;
    }

    public static function itemSell($item_id)
    {
        $item = Product::find($item_id);
        if (!$item) {
            return 'Success';
        }

        $global_settings = Marketplace::all();

        $export_item = 'Success';
        foreach ($global_settings as $global_setting) {
            $mp_item = MarketplaceItem::where([
                            'product_id' => $item_id,
                            'marketplace_type' => $global_setting->type
                        ])->first();

            if (!($item->type == 'numismatic' && $item->sell_price_method == 'Fixed' && $item->sell_premium > 0)) {
                continue;
            }

            if ($mp_item) {
                if ($item->entry_type == "bulk") {
                    // ----------- MP action update delete ---------- //
                    switch ($global_setting->type) {
                        case 'eBay':
                            if ($item->total_stock <= 0) {
                                $status = eBay::itemDelete($item);
                            } else {
                                $status = eBay::itemUpdate($item);
                            }
                            break;
                        case 'CoinCollector':
                            $status = 'Success';
                            break;
                        default:
                            $status = 'Success';
                            break;
                    }
                    if ($status !== 'Success') {
                        $export_item = 'Error';
                    }
                } elseif ($item->entry_type == "individual") {
                    switch ($global_setting->type) {
                        case 'eBay':
                            $status = eBay::itemDelete($item);
                            break;
                        case 'CoinCollector':
                            $status = CoinCollector::itemDelete($item);
                            break;
                        default:
                            $status = 'Success';
                            break;
                    }
                    if ($status !== 'Success') {
                        $export_item = 'Error';
                    }
                    // ----------- end MP action delete ---------- //
                }
            } else {
                $report = Report::where(['product_id' => $item_id, 'marketplace_type' => $global_setting->type])->first();
                if ($report) {
                    if ($item->total_stock <= 0) {
                        $report->delete();
                    } else {
                        $report->quantity = $item->total_stock;
                        $report->save();
                    }
                }
            }
        }

        return $export_item;
    }

    private static function itemDelete($item_id)
    {
        $item = Product::find($item_id);
        if (!$item) {
            return 'Success';
        }

        $global_settings = Marketplace::all();

        $export_item = 'Success';
        foreach ($global_settings as $global_setting) {
            $mp_item = MarketplaceItem::where([
                            'product_id' => $item_id,
                            'marketplace_type' => $global_setting->type
                        ])->first();

            if ($mp_item) {
                // ----------- MP action delete ---------- //
                switch ($global_setting->type) {
                    case 'eBay':
                        $find_ebay = FindEbayProduct::FindProduct((string)$mp_item->marketplace_id);
                        if ($find_ebay->Ack == "Success") {
                            if ($find_ebay->Item->ListingStatus == "Ended") {
                                $mp_item->delete();
                                $status = 'Success';
                            } else {
                                $status = eBay::itemDelete($item);
                            }
                        } else {
                            $mp_item->delete();
                            $status = 'Success';
                        }
                        break;
                    case 'CoinCollector':
                        $find_CC = CoinCollector::findItem($item);
                        if ($find_CC) {
                            $status = CoinCollector::itemDelete($item);
                        } else {
                            $mp_item->delete();
                            $status = 'Success';
                        }
                        break;
                    default:
                        $status = 'Success';
                        break;
                }
                if (!empty($status)) {
                    if ($status !== 'Success') {
                        $export_item = 'Error';
                    }
                }
                // ----------- end MP action delete ---------- //
            }

            $report = Report::where(['product_id' => $item_id, 'marketplace_type' => $global_setting->type])->first();
            if ($report) {
                $report->delete();
            }
        }

        return true;
    }
}
