<?php

namespace ppm;

use \DTS\eBaySDK\Constants;
use \DTS\eBaySDK\Trading\Services;
use \DTS\eBaySDK\Trading\Types;
use \DTS\eBaySDK\Trading\Enums;
use Illuminate\Support\Facades\Auth;

use Illuminate\Database\Eloquent\Model;
use ppm\Models\Setting;
use ppm\Models\Country;

class CreateEbayProduct extends Model
{
    public static function CreateProduct($params)
    {
        // $params["credentials"] - for UnitTest
        if (isset($params["credentials"])) {
            $appId = $params["credentials"]["appId"];
            $certId = $params["credentials"]["certId"];
            $devId = $params["credentials"]["devId"];
            $eBayAuthToken = $params["credentials"]["eBayAuthToken"];
            $paypal_email = 'example@example.com';
        } else {
            $ebay_keys = Marketplace::where('type', 'eBay')->first();

            if (!($ebay_keys && $ebay_keys->api_token)) {
                return ['error' => 'Not connect to eBay API. Check connection details.'];
            } else {
                $appId = $ebay_keys->app_id;
                $certId = $ebay_keys->cert_id;
                $devId = $ebay_keys->dev_id;
                $eBayAuthToken = $ebay_keys->api_token;
                $paypal_email = $ebay_keys->paypal_email;
            }
        }

        $service = new Services\TradingService([
            'siteId' => Constants\SiteIds::US,
            'sandbox' => env('SANDBOX'),
            'credentials' => [
                'appId'  => $appId,
                'certId' => $certId,
                'devId'  => $devId
            ]
        ]);

        // Create request for create Item in eBay
        $request = new Types\AddFixedPriceItemRequestType();
        $request->RequesterCredentials = new Types\CustomSecurityHeaderType();
        $request->RequesterCredentials->eBayAuthToken = $eBayAuthToken;

        if (!isset($params["credentials"])) {
            // ignore for unit test
            $ebay = CreateEbayProduct::getEbayMarketplaceState($params["product_id"], $params['entry_type'], $ebay_keys);
            if (!$ebay) {
                return ['error' => 'Export of this item is disabled. Check product export settings and global export settings.'];
            }
        } else {
            // for UnitTest
            $ebay['export'] = true;
            $ebay['pricing'] = 10;
            $ebay['shipping_cost'] = 10;
            $ebay['shipping_additional_cost'] = 1;
            $ebay['handling_time'] = 30;
            $ebay['shipping'] = 'USPSFirstClass';
            $ebay['pickup'] = true;
            $ebay['pickup_cost'] = 1.1;
            $ebay['pickup_additional_cost'] = 0.5;
            $ebay['global_shipping'] = true;
        }

        // create eBay item
        $item = new Types\ItemType();

        // 1. Title | Year, Mint Mark, Name (from catalog, not inventory), Grading Service, Grade, Variety, Designation
        // 1. Title | Description (Bulk Numismatic)
        $item->Title = substr($params['title'], 0, 80);

        // 2. Category | ePID in Aureus database
        $item->PrimaryCategory = new Types\CategoryType();
        $item->PrimaryCategory->CategoryID = $params['category_id'];

        // 3. Photos | images from Aureus database (first twelve only)
        if (isset($params['picture'])) {
            $item->PictureDetails = new Types\PictureDetailsType();
            $item->PictureDetails->GalleryType = Enums\GalleryTypeCodeType::C_GALLERY;

            $pictures_array = [];
            for ($pic_index = 0; $pic_index < count($params['picture']); $pic_index++) {
                array_push($pictures_array, $params['picture'][$pic_index]);
            }
            $item->PictureDetails->PictureURL = $pictures_array;
        }

        // 4. Items Specifics (For Unique Numismatic) (Bulk Numismatic - Circulated/Uncirculated, Certificate, Year)
        $item->ItemSpecifics = new Types\NameValueListArrayType();
        // 4.1. Certification | Grading Service
        if (!empty($params["certification"])) {
            $item->ItemSpecifics->NameValueList[] = new Types\NameValueListType([
                'Name' => 'Certification',
                'Value' => [$params["certification"]]
            ]);
        }
        // 4.2. Composition (select enter your own) | Metal
        if (!empty($params["composition"])) {
            $item->ItemSpecifics->NameValueList[] = new Types\NameValueListType([
                'Name' => 'Composition',
                'Value' => [$params["composition"]]
            ]);
        }
        // 4.3. Grade (select enter your own) | Grade
        if (!empty($params["grade"])) {
            $item->ItemSpecifics->NameValueList[] = new Types\NameValueListType([
                'Name' => 'Grade',
                'Value' => [$params["grade"]]
            ]);
        }
        // 4.4. Year | Year
        if (!empty($params["year"])) {
            $item->ItemSpecifics->NameValueList[] = new Types\NameValueListType([
                'Name' => 'Year',
                'Value' => [$params["year"]]
            ]);
        }
        // 4.5. Circulated/Uncirculated | alweys Unknown
        $item->ItemSpecifics->NameValueList[] = new Types\NameValueListType([
            'Name' => 'Circulated/Uncirculated',
            'Value' => ["Unknown"]
        ]);
        // 4.6. Strike Type (select enter your own) | Strike
        if (!empty($params["strike_type"])) {
            $item->ItemSpecifics->NameValueList[] = new Types\NameValueListType([
                'Name' => 'Strike Type',
                'Value' => [$params["strike_type"]]
            ]);
        }
        // 4.7. Mint Location (select enter your own) | Mint Mark
        if (!empty($params["mint_location"])) {
            $item->ItemSpecifics->NameValueList[] = new Types\NameValueListType([
                'Name' => 'Mint Location',
                'Value' => [$params["mint_location"]]
            ]);
        }
        // 4.8. Modified item | Leave blank
        // 4.9. Country / Region of Manufacture | Country (from catalog, not inventory)
        if (!empty($params["country"])) {
            $item->ItemSpecifics->NameValueList[] = new Types\NameValueListType([
                'Name' => 'Country',
                'Value' => [$params["country"]]
            ]);
        }

        // 5. Item Description | Comments field.
        // 5. Item Description | Description (from Catalog) (Bulk Numismatic)
        $item->Description = $params['description']; // HTML

        // 6. Format | Fixed Price
        $item->ListingType = Enums\ListingTypeCodeType::C_FIXED_PRICE_ITEM;

        // 7. Duration | Good till Cancelled
        $item->ListingDuration = Enums\ListingDurationCodeType::C_GTC;

        // 8. Price | Selling Price
        $price = number_format((double)$params['price'] * (1 + (double)$ebay['pricing'] / 100), 2, '.', '');

        //$price = (double)$params['price'];
        $item->StartPrice = new Types\AmountType(['value' => (double)$price]);
        $item->BestOfferDetails = new Types\BestOfferDetailsType();
        $item->BestOfferDetails->BestOfferEnabled = true;

        // 9. Quantity | 1 (For Unique Numismatic)
        // 9. Quantity | (Aureus field Total Available)
        if (isset($params['quantity'])) {
            $item->Quantity = (int)$params['quantity'];
        }

        // 10. Payment Options | Paypal (Payment Method in Aureus)
        $item->PaymentMethods = [
            'PayPal'
        ];
        $item->PayPalEmailAddress = $paypal_email;

        // 11. Sales Tax | I believe the checkbox is default checked. Leave it that way
        // 12. Return options | Leave both unchecked

        // 13. Shipping | Set to the default values as specified in the settings section.
        $item = CreateEbayProduct::createShipping($item, $price, $ebay, $params);

        if (isset($params["credentials"])) {
            // for test
            $item->Country = $params['location_country'];
        } else {
            $country = Country::getCountryAlpha2ByAlpha3($params['location_country']);
            $item->Country = $country;
        }

        $item->Location = $params['address'];

        if ($params['postal']) {
            $item->PostalCode = (string)$params['postal'];
        }

        // This is a required field
        $item->Currency = 'USD';

        // Specifies the maximum number of business days the seller commits to for preparing an item to be shipped after receiving a cleared payment.
        // This time does not include the shipping time (the carrier's transit time).
        // -- The item will be dispatched within 1 business days once payment has cleared --
        $item->DispatchTimeMax = $ebay['handling_time'];

        // This is a numeric identifier for an item's condition. All numeric Condition ID values map to an item condition string value. For example, numeric identifer 1000 maps to New condition.
        //$item->ConditionID = 1000;  // ??

        // Add Item to create request
        $request->Item = $item;

        // Add Item to eBay
        $response = $service->addFixedPriceItem($request);

        return $response; //for testing
    }

    static function createShipping($item, $price, $ebay, $params = null) {
        $item->ShippingDetails = new Types\ShippingDetailsType();
        $item->ShippingDetails->ShippingType = Enums\ShippingTypeCodeType::C_FLAT;

        $shippingService = new Types\ShippingServiceOptionsType();

        if ($ebay['shipping'] !== "ShipStation") {
            $shippingService->ShippingService = $ebay['shipping'];
                    //The base cost of shipping one unit of the item using the shipping service specified in the corresponding ShippingService field.
            $shippingService->ShippingServiceCost = new Types\AmountType(['value' => (double)$ebay["shipping_cost"]]);
                    // In the case of a multiple-quantity, fixed-price listing, the ShippingServiceAdditionalCost field also becomes applicable,
                    // and shows the cost to ship each additional unit of the item if the buyer purchases multiple quantity of the same line item
            $shippingService->ShippingServiceAdditionalCost = new Types\AmountType(['value' => (double)$ebay["shipping_additional_cost"]]);
        } else {
            $shippingService->ShippingService = 'Other';
            if (isset($params["credentials"])) {
                // for test
                $base_cost = 9.99;
                $cost_per_dollar = 1;
                $cost_per_pound = 1;
            } else {
                $base_cost = Setting::where(['form_name' => 'shipping', 'input_name' => 'base_cost'])->first()->value;
                $cost_per_dollar = Setting::where(['form_name' => 'shipping', 'input_name' => 'cost_per_dollar'])->first()->value;
                $cost_per_pound = Setting::where(['form_name' => 'shipping', 'input_name' => 'cost_per_pound'])->first()->value;
            }

            $additionalCost = 0;

            if (!$base_cost) {
                $base_cost = 0.0;
            }
            if (!$cost_per_dollar) {
                $cost_per_dollar = 0;
            }
            if (!$cost_per_pound) {
                $cost_per_pound = 0;
            }

            $cost_per_dollar !== 0
                ? $additionalCost = (double)(($price * $cost_per_dollar) / 100)
                : $additionalCost = 0;

            $shippingService->ShippingServiceCost = new Types\AmountType(['value' => (double)$base_cost]);
            $shippingService->ShippingServiceAdditionalCost = new Types\AmountType(['value' => (double)$additionalCost]);
        }

        $item->ShippingDetails->ShippingServiceOptions[] = $shippingService;

        // Pickup
        if ($ebay['pickup']) {
            $shippingService = new Types\ShippingServiceOptionsType();
            $shippingService->ShippingService = 'Pickup';
            $shippingService->ShippingServiceCost = new Types\AmountType(['value' => (double)$ebay['pickup_cost']]);
            $shippingService->ShippingServiceAdditionalCost = new Types\AmountType(['value' => (double)$ebay['pickup_additional_cost']]);
            $item->ShippingDetails->ShippingServiceOptions[] = $shippingService;
        }

        if ($ebay['global_shipping']){
            $item->ShippingDetails->GlobalShipping = true;
        } else {
            $item->ShippingDetails->GlobalShipping = false;
        }

        return $item;
    }

    static function getEbayMarketplaceState($id, $type, $ebay_keys) {
        // default settings
        if ($type == 'bulk') {
            if ((bool)$ebay_keys->bnum) {
                $ebay['status'] = (bool)$ebay_keys->bnum;
                $ebay['shipping'] = $ebay_keys->bnum_sh;
                $ebay['export'] = true;
                $ebay['pricing'] = $ebay_keys->bnum_pr;
                $ebay['shipping_cost'] = $ebay_keys->bnum_sh_cost;
                $ebay['shipping_additional_cost'] = $ebay_keys->bnum_sh_add_cost;
                $ebay['handling_time'] = $ebay_keys->bnum_handling_time;
                $ebay['pickup'] = $ebay_keys->bnum_pickup;
                $ebay['pickup_cost'] = $ebay_keys->bnum_pickup_cost;
                $ebay['pickup_additional_cost'] = $ebay_keys->bnum_pickup_additional_cost;
            }
        } elseif ($type == 'individual') {
            if ((bool)$ebay_keys->inum) {
                $ebay['status'] = (bool)$ebay_keys->inum;
                $ebay['shipping'] = $ebay_keys->inum_sh;
                $ebay['export'] = true;
                $ebay['pricing'] = $ebay_keys->inum_pr;
                $ebay['shipping_cost'] = $ebay_keys->inum_sh_cost;
                $ebay['shipping_additional_cost'] = $ebay_keys->inum_sh_add_cost;
                $ebay['handling_time'] = $ebay_keys->inum_handling_time;
                $ebay['pickup'] = $ebay_keys->inum_pickup;
                $ebay['pickup_cost'] = $ebay_keys->inum_pickup_cost;
                $ebay['pickup_additional_cost'] = $ebay_keys->inum_pickup_additional_cost;
            }
        }

        $ebay['global_shipping'] = true;

        // if edit coin
        if ($id) {
            $marketplace = MarketplaceItemSetting::where(['product_id' => $id, 'marketplace' => 'eBay'])->first();

            if ($marketplace) {
                $ebay['export'] = (bool)$marketplace->export;
                $ebay['shipping'] = $marketplace->shipping;
                $ebay['pricing'] = $marketplace->pricing;
                $ebay['shipping_cost'] = $marketplace->shipping_cost;
                $ebay['shipping_additional_cost'] = $marketplace->shipping_additional_cost;
                $ebay['handling_time'] = $marketplace->handling_time;
                $ebay['pickup'] = $marketplace->pickup;
                $ebay['pickup_cost'] = $marketplace->pickup_cost;
                $ebay['pickup_additional_cost'] = $marketplace->pickup_additional_cost;
                $ebay['global_shipping'] = $marketplace->global_shipping;
            }
        }

        if ($ebay['status'] && $ebay['export']) {
            return $ebay;
        }

        return false;
    }
}
