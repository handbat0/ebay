<?php

namespace ppm\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Eloquent\Model;
use ppm\Report;
use ppm\Api\V1\Controllers\ReportsController;

class MarketplaceRepeatReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'marketplace_report:repeat';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Repeat in_progress status report';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *s
     * @return mixed
     */
    public function handle()
    {
        $min = Report::where('status', 'in_progress')->min('retries');
        $report = Report::where(['status' => 'in_progress', 'retries' => $min])->limit(50)->get();
        $count = count($report);
        $controller = app()->make(ReportsController::Class);
        if ($count) {
            for ($i = 0; $i < $count; $i++) {
                if ($report[$i]->status == 'in_progress') {
                    $report[$i]->retries = $report[$i]->retries + 1;
                    $report[$i]->save();
                    app()->call([$controller, "autoRepeatMarketplaceReports"], [$report[$i]->id]);
                }
            }
        }
    }
}
