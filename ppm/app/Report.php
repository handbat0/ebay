<?php

namespace ppm;

use Illuminate\Database\Query\Builder;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $fillable = [
        'product_id',
        'type',
        'status',
        'message',
        'data',
        'marketplace_type',
        'name',
        'quantity',
        'expiration_date',
        'link',
        'retries',
    ];

    public function scopeForProductId(Builder $query, $product_id)
    {
        return $query->where('product_id', $product_id);
    }

    // public function scopeForProductType(Builder $query, $product_type)
    // {
    //     return $query->where('products.type', $product_type);
    // }

    public function scopeForProductEntryType(Builder $query, $product_type)
    {
        $entry_type = 'bulk';
        if ($product_type == 'inum') {
            $entry_type = 'individual';
        }
        return $query->where('products.entry_type', $entry_type);
    }

    public function scopeForType(Builder $query, $type)
    {
        return $query->where('type', $type);
    }

    public function scopeForStatus(Builder $query, $status)
    {
        return $query->where('status', $status);
    }

    public function scopeForMarketplaceType(Builder $query, $marketplace_type)
    {
        return $query->where('marketplace_type', $marketplace_type);
    }

    public function scopeForName(Builder $query, $name)
    {
        return $query->where('name', $name);
    }

    public function scopeForQuantity(Builder $query, $quantity)
    {
        return $query->where('quantity', $quantity);
    }

    public function scopeForExpirationDate(Builder $query, $expiration_date)
    {
        return $query->where('expiration_date', $expiration_date);
    }

    public function scopeForRetries(Builder $query, $retries)
    {
        return $query->where('retries', $retries);
    }
}
