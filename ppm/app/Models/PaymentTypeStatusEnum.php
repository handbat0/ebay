<?php

namespace ppm\Models;

class PaymentTypeStatusEnum
{
    const ACTIVE   = 'Active';
    const INACTIVE = 'Inactive';

    const ONLINE_ACTIVE   = 'Yes';
    const ONLINE_INACTIVE = 'No';
}
