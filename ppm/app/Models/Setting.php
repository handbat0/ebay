<?php

namespace ppm\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Setting extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'form_name',
        'input_name',
        'value'
    ];

    public function getAllSettings()
    {
        $result = [];

        foreach ($this->newQuery()->get()->toArray() as $row) {
            if (!isset($result[$row['form_name']])) {
                $result[$row['form_name']] = [];
            }

            $result[$row['form_name']][$row['input_name']] = $row['value'] ? $row['value'] : null;
        }

        return $result;
    }

    protected function setValueAttribute($val)
    {
        switch ($this->value_type) {
            case 'array':
                $val = json_encode((array) $val);
                break;
        }
        $this->attributes['value'] = (string) $val;
    }

    public function toArray()
    {
        $data = array_except(parent::toArray(), [ 'id' ]);

        switch ($data['value_type']) {
            case 'number':
                $data['value'] = floatval($data['value']);
                break;
            case 'integer':
                $data['value'] = intval($data['value']);
                break;
            case 'array':
                $data['value'] = json_decode($data['value'], true);
                break;
            case 'date':
                $data['value'] = Carbon::parse($data['value'])->toDateString();
                break;
            case 'datetime':
                $data['value'] = Carbon::parse($data['value'])->toDateTimeString();
                break;
        }

        return $data;
    }

    public function save(array $options = [])
    {
        Cache::forget('settings');
        return parent::save();
    }
}
