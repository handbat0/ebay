<?php

namespace ppm\Models;

use Illuminate\Database\Eloquent\Model;

class ProductRedBookCategory extends Model
{
    public $timestamps = false;

    protected $table = 'products_red_book_categories';

    protected $fillable = [
        'product_id',
        'red_book_category_id',
    ];

    protected $casts = [
        'product_id'           => 'integer',
        'red_book_category_id' => 'integer'
    ];
}
