<?php

namespace ppm\Models;

use Illuminate\Database\Eloquent\Model;

class RoleLimit extends Model
{
    protected $table = 'role_limits';
    public $timestamps = false;

    protected $fillable = [ 'role_id', 'key', 'value' ];

    public function role()
    {
        return $this->belongsTo(Role::class);
    }
}
