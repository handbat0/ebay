<?php

namespace ppm\Models;

use Illuminate\Database\Eloquent\Model;
use ppm\Models\Orders\Order;
use ppm\Models\Products\Location;

class Shipment extends Model
{
    protected $fillable = [
        'location_id',
        'address_id',
        'ship_from',
        'ship_to',
        'carrier',
        'service',
        'packages',
        'awb',
        'status',
        'details',
        'carrier_api_response'
    ];

    protected $casts = [
        'ship_from'            => 'array',
        'ship_to'              => 'array',
        'packages'             => 'array',
        'details'              => 'array',
        'carrier_api_response' => 'array',
    ];

    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class);
    }

    public function toArray()
    {
        $arr                 = array_except(parent::toArray(), ['updated_at', 'carrier_api_response']);
        $arr['tracking_url'] = $this->getTrackingUrlForCode($this->awb);

        foreach ($arr['packages'] as $index => $package) {
            $trackingCode                            = $this->getTrackingCodeForPackage($index);
            $arr['packages'][$index]['awb']          = $trackingCode;
            $arr['packages'][$index]['tracking_url'] = $this->getTrackingUrlForCode($trackingCode);
        }
        if (!$arr['packages'][0]['awb']) {
            $arr['packages'][0]['awb'] = $this->awb;
            $arr['packages'][0]['tracking_url'] = $this->getTrackingUrlForCode($this->awb);
        }

        $arr['carrier'] = strtoupper(str_replace('shipstation:', '', $arr['carrier']));
        $arr['service'] = strtoupper(str_replace('_', ' ', $arr['service']));

        return $arr;
    }

    public function getTrackingCodeForPackage($index)
    {
        if ( ! $this->carrier_api_response) {
            return null;
        }

        switch ($this->carrier) {
            case 'UPS':
                if (array_get($this->carrier_api_response, "PackageResults.{$index}")) {
                    $awb = array_get($this->carrier_api_response, "PackageResults.{$index}.TrackingNumber");
                } else {
                    $awb = array_get($this->carrier_api_response, "PackageResults.TrackingNumber");
                }
                break;
            case 'USPS':
                break;
            // shipstation
            default:
                $awb = array_get($this->carrier_api_response, 'trackingNumber');
        }

        return $awb;
    }

    public function getTrackingUrlForCode($code)
    {
        if ( ! $code) {
            return null;
        }

        switch ($this->carrier) {
            case 'UPS':
                return sprintf(
                    'https://wwwapps.ups.com/WebTracking/processInputRequest?tracknum=%s&Requester=trkinppg',
                    $code
                );
            case 'shipstation:ups':
                return sprintf(
                    'https://wwwapps.ups.com/WebTracking/processInputRequest?tracknum=%s&Requester=trkinppg',
                    $code
                );
        }

        return null;
    }

    protected function getLabelFormatAttribute()
    {
        switch ($this->carrier) {
            case 'UPS':
                return strtolower(array_get($this->carrier_api_response, 'PackageResults.LabelImage.LabelImageFormat.Code'));
            default:
                return 'pdf';
        }
    }

    protected function getLabelContentAttribute()
    {
        switch ($this->carrier) {
            case 'UPS':
                return array_get($this->carrier_api_response, 'PackageResults.LabelImage.GraphicImage');
            default:
                return array_get($this->carrier_api_response, 'labelData');
        }
    }
}
