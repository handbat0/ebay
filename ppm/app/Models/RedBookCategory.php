<?php

namespace ppm\Models;

use Illuminate\Database\Eloquent\Model;
use ppm\Models\Products\Product;

class RedBookCategory extends Model
{
    public $timestamps = false;

    protected $table = 'red_book_categories';

    protected $fillable = [
        'parent_id',
        'name',
        'ebay_category_id',
        'CC_code',
    ];

    protected $casts = [
        'parent_id'        => 'integer',
        'ebay_category_id' => 'integer'
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class, 'products_red_book_categories');
    }

    public function toArray()
    {
        return array_except(parent::toArray(), [ 'created_at', 'updated_at' ]);
    }

    public function getParentName()
    {
        $parent = RedBookCategory::find($this->parent_id);
        if ($parent) {
            return $parent->name;
        } else {
            return null;
        }
    }
}
