<?php

namespace ppm\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ReviewComment extends Model
{
    protected $table = 'review_comments';

    protected $fillable = [
        'review_id',
        'user_id',
        'date',
        'content'
    ];

    protected $casts = [
        'review_id' => 'integer',
        'user_id'   => 'integer'
    ];

    protected $dates = ['date'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function comment()
    {
        return $this->belongsTo(Review::class);
    }

    public function save(array $options = [ ])
    {
        $this->setDateForCurrentDateTime();

        return parent::save($options);
    }

    public function setDateForCurrentDateTime()
    {
        if (array_get($this->attributes, 'date')) {
            return;
        }

        $this->attributes['date'] = Carbon::now()->toDateTimeString();

        return $this->attributes['date'];
    }
}
