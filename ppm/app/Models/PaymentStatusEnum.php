<?php

namespace ppm\Models;

class PaymentStatusEnum
{
    const PAID           = 'Paid';
    const CLEARED        = 'Cleared';
    const ON_HOLD_FUNDS  = 'On Hold Funds';
    const TO_BE_RECEIVED = 'To be received';
    const CANCELLED      = 'Cancelled';
}
