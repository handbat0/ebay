<?php

namespace ppm\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use ppm\Models\Products\Product;

class ProductView extends Model
{
    public $timestamps = false;

    protected $table = 'product_views';

    protected $fillable = [
        'product_id',
        'user_id',
        'session_id',
        'view_date',
        'ip_address'
    ];

    protected $casts = [
        'product_id' => 'integer',
        'user_id'    => 'integer'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function save(array $options = [ ])
    {
        $this->viewDateIsSetForCurrentDatetime();
        $this->requestIpAddressFromServer();
        $this->sessionIdIsSetForNullIfUserIdIsPresent();

        return parent::save($options);
    }

    public function viewDateIsSetForCurrentDatetime()
    {
        if (array_get($this->attributes, 'view_date')) {
            return;
        }

        $this->attributes['view_date'] = Carbon::now()->toDateTimeString();

        return $this->attributes['view_date'];
    }

    public function sessionIdIsSetForNullIfUserIdIsPresent()
    {
        if (array_get($this->attributes, 'user_id')) {
            $this->attributes['session_id'] = null;
        }

        return $this->attributes['session_id'];
    }

    public function requestIpAddressFromServer()
    {
        if (array_get($this->attributes, 'ip_address')) {
            return;
        }

        $this->attributes['ip_address'] = $_SERVER['SERVER_ADDR'];

        return $this->attributes['ip_address'];
    }
}
