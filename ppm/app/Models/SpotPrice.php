<?php

namespace ppm\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use ppm\Models\Products\Material;

class SpotPrice extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'material_id',
        'date',
        'ask_price',
        'bid_price',
        'change'
    ];

    protected $casts = [
        'material_id' => 'integer',
        'date'        => 'datetime',
        'ask_price'   => 'double',
        'bid_price'   => 'double',
        'change'      => 'double'
    ];

    protected static $cache = [ ];

    public static function getAvailableAt($materialId, $date)
    {
        if ($date == null) {
            $date = Carbon::now();
        } else {
            $date = new Carbon($date);
        }

        $date     = $date->format('Y-m-d H:i:00');
        $cacheKey = $materialId . '-' . $date;

        if (!array_key_exists($cacheKey, static::$cache)) {
            $price = SpotPrice::where('material_id', '=', $materialId)
                ->where('date', '<=', $date)
                ->orderBy('date', 'DESC')
                ->first();

            if (Config::get('app.env') == 'testing') {
                return $price;
            }

            static::$cache[$cacheKey] = $price;
        }

        return static::$cache[$cacheKey];
    }

    public function scopeAvailableAt(Builder $query, $date)
    {
        $date = (string) $date;
        $query->join(
            DB::raw(
                '(SELECT material_id, MAX(date) AS date FROM spot_prices WHERE date <= ' .
                DB::connection()->getPdo()->quote($date) .
                ' GROUP BY material_id) AS max_dates'
            ),
            function (JoinClause $join) {
                $join->on('spot_prices.material_id', '=', 'max_dates.material_id')
                     ->on('spot_prices.date', '=', 'max_dates.date');

                return $join;
            }
        );

        return $query;
    }

    public function scopeForMaterial(Builder $query, $material_id)
    {
        return $query->where('material_id', $material_id);
    }

    public function scopeForEndDate(Builder $query, $end_date)
    {
        if ($end_date == null) {
            $end_date = Carbon::now()->toDateTimeString();
        }

        return $query->where('date', '<=', $end_date);
    }

    public function scopeForStartDate(Builder $query, $start_date)
    {
        if ($start_date == null) {
            $start_date = Carbon::yesterday()->toDateTimeString();
        }

        return $query->where('date', '>=', $start_date);
    }

    public function material()
    {
        return $this->belongsTo(Material::class);
    }

    public function toArray()
    {
        return array_except(parent::toArray(), [ 'created_at', 'updated_at' ]);
    }
}
