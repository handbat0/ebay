<?php

namespace ppm\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    public $timestamps = false;

    protected $fillable = [ 'name' ];

    public function toArray()
    {
        return array_except(parent::toArray(), ['pivot']);
    }
}
