<?php

namespace ppm\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'is_admin'
    ];

    protected $casts = [
        'is_admin' => 'boolean',
    ];

    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'role_permissions');
    }

    public function limits()
    {
        return $this->hasMany(RoleLimit::class);
    }

    public function toArray()
    {
        $arr = array_except(parent::toArray(), [ 'created_at', 'updated_at', 'deleted_at', 'pivot' ]);

        return $arr;
    }
}
