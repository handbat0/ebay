<?php

namespace ppm\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use ppm\Models\ExternalReference\QuickbooksReference;

class PaymentType extends Model
{
    use SoftDeletes;

    const ACTIVE_TRUE  = 'Active';
    const ACTIVE_FALSE = 'Inactive';

    const ONLINE_STATUS_TRUE  = 'Yes';
    const ONLINE_STATUS_FALSE = 'No';


    protected $fillable = [
        'name',
        'status',
        'clear_time',
        'is_cash',
        'is_printable',
        'category',
        'svg_name',
        'online_status_person',
        'online_status_ship',
        'online_lower_person',
        'online_lower_ship',
        'online_upper_person',
        'online_upper_ship',
        'remittance_time',
        'order_holdout_percentage',
        'order_holdout_days',
        'overcharge',
        'product_overcharges'
    ];

    protected $casts = [
        'clear_time'               => 'integer',
        'is_cash'                  => 'boolean',
        'is_printable'             => 'boolean',
        'remittance_time'          => 'integer',
        'order_holdout_percentage' => 'integer',
        'order_holdout_days'       => 'integer',
        'product_overcharges'      => 'json'
    ];

    public function qbReference()
    {
        return $this->morphOne(QuickbooksReference::class, 'referenceable');
    }

    public function scopeAvailableForOnline(Builder $query)
    {
        $query->whereRaw(
            'status = ? and (online_status_person = ? or online_status_ship = ?)',
            [PaymentTypeStatusEnum::ACTIVE, PaymentTypeStatusEnum::ONLINE_ACTIVE, PaymentTypeStatusEnum::ONLINE_ACTIVE]
        );

        return $query;
    }

    public function getOverchargeForProductType($type)
    {
        return array_get($this->product_overcharges, $type, 0);
    }

    public function isAllowedForDeliveryModeAndAmount($deliveryMode, $amount)
    {
        return $this->status == static::ACTIVE_TRUE &&
               $this->getAttribute('online_status_' . $deliveryMode) == static::ONLINE_STATUS_TRUE &&
               ($this->getAttribute('online_lower_' . $deliveryMode) === null ||
                $this->getAttribute('online_lower_' . $deliveryMode) <= $amount) &&
               ($this->getAttribute('online_upper_' . $deliveryMode) === null ||
                $this->getAttribute('online_upper_' . $deliveryMode) >= $amount);
    }
}
