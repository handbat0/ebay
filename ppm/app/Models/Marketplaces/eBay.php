<?php

namespace ppm\Models\Marketplaces;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use ppm\Models\RedBookCategory;
use ppm\Models\Products\Product;
use ppm\Models\Products\Material;
use ppm\Models\Products\ProductImage;
use ppm\Models\Products\Location;

use ppm\Marketplace;
use ppm\MarketplaceItemSetting;
use ppm\MarketplaceItem;
use ppm\Report;
use ppm\ReportError;

use ppm\CreateEbayProduct;
use ppm\FindEbayProduct;
use ppm\UpdateEbayProduct;
use ppm\EndEbayProduct;

use \DTS\eBaySDK\Constants;
use \DTS\eBaySDK\Trading\Services;
use \DTS\eBaySDK\Trading\Types;
use \DTS\eBaySDK\Trading\Enums;

class eBay extends Model
{
    const MP_TYPE = 'eBay';

    // create or update quantity (for bulk)
    public static function itemCreate(Product $item)
    {
        $ebay_params = eBay::fillEbayItem($item);
        $response = CreateEbayProduct::CreateProduct($ebay_params);
        if ($response->Ack !== "Failure") {
            // add ebay id to DB
            $mp_item = new MarketplaceItem;
            $mp_item->product_id = $ebay_params['product_id'];
            $mp_item->marketplace_id = $response->ItemID;
            $mp_item->marketplace_type = eBay::MP_TYPE;
            $mp_item->save();
        }
        return eBay::report($response, 'create', $ebay_params)['status'];
    }

    // create or update quantity (for bulk)
    public static function itemUpdate(Product $item)
    {
        $ebay_params = eBay::fillEbayItem($item);
        $response = UpdateEbayProduct::UpdateProduct($ebay_params);
        return eBay::report($response, 'update', $ebay_params)['status'];
    }

    public static function itemDelete(Product $item)
    {
        $ebay_params = eBay::fillEbayItem($item);
        $response = EndEbayProduct::EndProduct(strval($ebay_params['ebay_id']));
        $status = eBay::report($response, 'delete', $ebay_params)['status'];
        if ($status == "Success") {
            $mp_item = MarketplaceItem::where('marketplace_id', $ebay_params['ebay_id'])->first();
            $mp_item->delete();

            $report = Report::where(['product_id' => $item->id, 'marketplace_type' => eBay::MP_TYPE])->first();
            if ($report) {
                $report->delete();
            }
        }
        return $status;
    }

    public static function getEbayTime($credentials)
    {
        if (!$credentials) {
            return false;
        }

        $service = eBay::getTradingService($credentials);

        $request = new Types\GeteBayOfficialTimeRequestType();
        $request->RequesterCredentials = new Types\CustomSecurityHeaderType();
        $request->RequesterCredentials->eBayAuthToken = $credentials['eBayAuthToken'];

        $response = $service->geteBayOfficialTime($request);

        if ($response->Ack == 'Success') {
            return true;
        } else {
            return false;
        }
    }

    private static function getTradingService($credentials)
    {
        $service = new Services\TradingService([
            'siteId' => Constants\SiteIds::US,
            'sandbox' => env('SANDBOX'),
            'credentials' => [
                'appId'  => $credentials['appId'],
                'certId' => $credentials['certId'],
                'devId'  => $credentials['devId']
            ],
        ]);
        return $service;
    }

    public static function report($response, $event, $params)
    {
        $report = Report::where(['product_id' => $params["product_id"], 'marketplace_type' => eBay::MP_TYPE])->first();
        $params['type'] = $event;
        $params['marketplace_type'] = eBay::MP_TYPE;
        $params['message'] = null;
        $params['name'] = $params['title'];
        $status = 'in_progress';
        $count_limit = 50;
        $params['expiration_date'] = null;
        $params['link'] = null;
        $endItemFromEbay = false;
        $sell_item = false;

        if ($event == 'delete') {
            $params['quantity'] = 0;
        }

        if (!empty($response->Ack)) {
            $eBayStatus = $response->Ack;
        } else {
            $eBayStatus = 'error';
            $params['message'] = $response['error'];
        }

        if (!empty($response->Errors)) {
            foreach ($response->Errors as $error) {
                if ($error->ErrorCode == '291') {
                    $endItemFromEbay = true;
                } elseif ($error->ErrorCode == '1047') {
                    $sell_item = true;
                }
                $params['message'] = $params['message'] . $error->ErrorCode . ': ' . $error->LongMessage . PHP_EOL;
                $report_error = ReportError::where('error_code', $error->ErrorCode)->first();
                if ($report_error) {
                    if ($error->SeverityCode == 'Error') {
                        $report_error->increment('count');
                        $report_error->save();
                        if ($report_error->count >= $count_limit) {
                            $status = 'error';
                        }
                    }
                } else {
                    if ($error->ErrorCode == '291') {
                        $endItemFromEbay = true;
                    }
                    ReportError::create([
                        'error_code' => $error->ErrorCode,
                        'message' => $error->LongMessage,
                        'mp_type' => eBay::MP_TYPE,
                    ]);
                }
            }
        }

        if ($report && $sell_item) {
            $marketplace_item = MarketplaceItem::where(['product_id' => $params["product_id"], 'marketplace_type' => eBay::MP_TYPE])->first();
            if ($marketplace_item) {
                $marketplace_item->delete();
            }
            $report->delete();
            return ['status' => 'Success', 'endItemFromEbay' => $endItemFromEbay];
        }

        if ($eBayStatus == 'Success') {
            $params['message'] = 'Item was successfully ' . $event . 'd.';
            if ($event == 'delete') {
                if ($report) {
                    $report->delete();
                    return ['status' => $eBayStatus, 'endItemFromEbay' => $endItemFromEbay];
                }
            }
            $params['status'] = 'active';
            if (!empty($response->EndTime)) {
                $params['expiration_date'] = $response->EndTime;
            }
        } elseif ($eBayStatus == 'Warning') {
            $params['status'] = 'active';
            if (!empty($response->EndTime)) {
                $params['expiration_date'] = $response->EndTime;
            }
        } else {
            $params['status'] = $status;
        }

        if (($eBayStatus == 'Success' || $eBayStatus == 'Warning') && $event !== 'delete') {
            $find = FindEbayProduct::FindProduct((string)$response->ItemID);
            $params['link'] = $find->Item->ViewItemURLForNaturalSearch;
        }

        if ($report) {
            $report->type = $report->type === 'delete' ? 'delete' : $params['type'];
            $report->status = $params['status'];
            $report->message = $params['message'];
            $report->name = $params['name'];
            $report->quantity = $params['quantity'];
            $report->link = $params['link'] ? $params['link'] : $report->link;
            $report->expiration_date = $params['expiration_date'] ? $params['expiration_date'] : $report->expiration_date;
            $report->save();
        } else {
            Report::create($params);
        }

        return ['status' => $eBayStatus, 'endItemFromEbay' => $endItemFromEbay];
    }

    private static function fillEbayItem(Product $item)
    {
        $grade = null;
        $year = null;
        $strike_type = null;
        $mint_location = null;
        $material_name = null;
        $country = null;
        $category_id = null;
        $ebay_id = null;

        // address
        if (Auth::user()) {
            $location = Location::find(Auth::user()->location_id);
        } else {
            $location = Location::first();
        }

        if ($location->state !== '') {
            $address = $location->city.', '.$location->state.', '.$location->address_1;
        } else {
            $address = $location->city.', '.$location->address_1;
        }

        // price
        $price = number_format($item->sell_premium, 2, '.', '');

        // description
        if ($item->shopping_cart_description) {
            $description = '<p>' . $item->shopping_cart_description . '</p>';
        } else {
            $description = '<p></p>';
        }

        if ($item->entry_type == 'individual') {
            $parent = Product::find($item->parent_id);

            // grade
            $grade = $item->num_grade;

            // year
            $year = $item->num_year;

            // strike_type
            $strike_type = $item->num_strike;

            // mint_location
            $mint_location = $item->num_mint_mark;

            // title individual
            $title = Marketplace::individualTitle(
                $year,
                $mint_location,
                $parent->name,
                $item->num_grading_agency,
                $grade,
                $item->num_variety,
                $item->num_designation
            );

            // composition
            $material_id = $item->material_id;
            $material_name = Material::find($item->material_id)->name;

            // country
            $country = $parent->country_name;

            // certification individual
            if ($item->num_grading_agency == 'PCGS' && $item->num_spec_prop == 'None') {
                $certification = $item->num_grading_agency;
            } elseif ($item->num_grading_agency == 'PCGS' && $item->num_spec_prop !== 'None') {
                $certification = 'PCGS and CAC';
            } elseif ($item->num_grading_agency == 'NGC' && $item->num_spec_prop == 'None') {
                $certification = $item->num_grading_agency;
            } elseif ($item->num_grading_agency == 'NGC' && $item->num_spec_prop !== 'None') {
                $certification = 'NGC and CAC';
            } else {
                $certification = $item->num_grading_agency;
            }

            $img_path = '/media/';

            $parent_category_id = eBay::getCategoryID($item->parent_id);

            if (!empty($parent_category_id)) {
                $category_id = RedBookCategory::find($parent_category_id)->ebay_category_id;
            }
        } else {
            // title bulk (crop after the first 80 characters)
            $title = Marketplace::individualTitle($item->description);

            $img_path = '/media/img/products/';

            $redbook_category_id = eBay::getCategoryID($item->id);

            $parent_category_name = '';
            if (!empty($redbook_category_id)) {
                $category = RedBookCategory::find((int)$redbook_category_id);
                if ($category) {
                    $category_id = $category->ebay_category_id;
                    $parent_category_name = $category->getParentName();
                }
            }

            // certification bulk
            if (strpos($title, 'PCGS') !== false) {
                $certification = 'PCGS';
            } elseif (strpos($title, 'NGC') !== false) {
                $certification = 'NGC';
            } else {
                if ($parent_category_name == 'World Coins') {
                    $certification = 'Uncertified';
                } else {
                    $certification = 'U.S. Mint';
                }
            }
        }

        // category_id
        if ($category_id) {
            $category_id = strval($category_id);
        } else {
            $category_id = '169305';
        }

        // picture
        $url = env('APP_URL');
        $picture = [];
        $product_imgs = ProductImage::where('product_id', $item->id)->get();
        foreach ($product_imgs as $product_img_index => $product_img) {
            $picture[$product_img_index] = $url . $img_path . $product_img->path;
            if(count($picture) >= 12) {
                break;
            }
        }

        // eBay ID
        $mp_item = MarketplaceItem::where([
            'product_id' => $item->id,
            'marketplace_type' => eBay::MP_TYPE
        ])->first();
        if ($mp_item) {
            $ebay_id = $mp_item->marketplace_id;
        }

        $params = [
            'product_id' => $item->id,
            'entry_type' => $item->entry_type,
            'title' => $title,
            'category_id' => $category_id,
            'picture' => $picture,
            'certification' => $certification,
            'composition' => $material_name,
            'grade' => $grade,
            'year' => $year,
            'strike_type' => $strike_type,
            'mint_location' => $mint_location,
            'country' => $country,
            'description' => $description,
            'price' => $price,
            'quantity' => $item->total_stock,
            'location_country' => $location->country,
            'address' => $address,
            'postal' => $location->zip,
            'ebay_id' => $ebay_id,
        ];

        return $params;
    }

    private static function getCategoryID($item_id)
    {
        $product_categories = DB::table('products_red_book_categories')->where('product_id', $item_id)->get();
        $count_product_categories = count($product_categories);

        $redbook_category_id = null;
        switch ($count_product_categories) {
            case 1:
                $redbook_category_id = $product_category->red_book_category_id;
                break;
            case 2:
            case 3:
                foreach ($product_categories as $product_category) {
                    if ($redbook_category_id < $product_category->red_book_category_id) {
                        $redbook_category_id = $product_category->red_book_category_id;
                    }
                }
                break;
            default:
                break;
        }

        return $redbook_category_id;
    }
}
