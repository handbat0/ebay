<?php

namespace ppm\Models\Marketplaces;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use ppm\Models\Products\Product;
use ppm\Models\Products\Material;
use ppm\Models\Products\ProductImage;

use ppm\Models\RedBookCategory;

use ppm\Marketplace;
use ppm\MarketplaceItemSetting;
use ppm\MarketplaceItem;

use ppm\Report;
use ppm\ReportError;

class CoinCollector extends Model
{
    const MP_TYPE = 'CoinCollector';

    public static function itemCreate(Product $item)
    {
        $create = CoinCollector::callCoinCollector($item, 'create');

        $params = CoinCollector::fillParam($item);
        $status = CoinCollector::report($create, 'create', $params);

        if ($status == 'Success') {
            $mp_item = new MarketplaceItem;
            $mp_item->product_id = $item->id;
            $mp_item->marketplace_id = $item->id;
            $mp_item->marketplace_type = CoinCollector::MP_TYPE;
            $mp_item->save();
        }

        return $status;
    }

    public static function itemUpdate(Product $item)
    {
        $update = CoinCollector::callCoinCollector($item, 'update');

        $params = CoinCollector::fillParam($item);
        $status = CoinCollector::report($update, 'update', $params);

        return $status;
    }

    public static function itemDelete(Product $item)
    {
        $delete = CoinCollector::callCoinCollector($item, 'delete');

        $params = CoinCollector::fillParam($item);
        $status = CoinCollector::report($delete, 'delete', $params);

        if ($status == 'Success') {
            $mp_item = MarketplaceItem::where('marketplace_id', $item->id)->first();
            $mp_item->delete();

            $report = Report::where(['product_id' => $item->id, 'marketplace_type' => CoinCollector::MP_TYPE])->first();
            if ($report) {
                $report->delete();
            }
        }

        return $status;
    }

    public static function findItem(Product $item)
    {
        return $find = CoinCollector::callCoinCollector($item, 'get');
        if ($find->GetCoinListingByDPNResult->Success) {
            return true;
        }
        return false;
    }

    public static function callCoinCollector(Product $item, $event)
    {
        ini_set('default_socket_timeout', '1000');
        $options = array(
            'cache_wsdl'   => WSDL_CACHE_NONE,
            'trace'        => true,
            'exceptions'   => false,
            'soap_version' => SOAP_1_2,
        );

        $client = new \SoapClient('https://dealer.collectorscorner.com/WebServices/CCListings.asmx?wsdl', $options);

        $global_settings = Marketplace::where('type', CoinCollector::MP_TYPE)->first();

        $auth = [
            'Username' => $global_settings->u_name,
            'Password' => $global_settings->u_pwd
        ];

        $header = new \SoapHeader('http://dealer.collectorscorner.com/webservices', 'AuthenticationSoapHeader', $auth, false);
        $client->__setSoapHeaders($header);

        switch ($event) {
            case 'create':
            case 'update':
                $param = CoinCollector::fillParam($item);
                $result = $client->InsertCoinListingByCriteria($param);
                break;
            case 'delete':
                $param = ['DealerProductNumber' => $item->id];
                $result = $client->DeleteCoinListingByDPN($param);
                break;
            case 'get':
                $param = ['DealerProductNumber' => $item->id];
                $result = $client->GetCoinListingByDPN($param);
                break;
            default:
                $result = 'Unknow event';
                break;
        }

        return $result;
    }

    private static function report($response, $event, $params)
    {
        $report = Report::where(['product_id' => $params["DealerProductNumber"], 'marketplace_type' => CoinCollector::MP_TYPE])->first();

        $params["product_id"] = $params["DealerProductNumber"];
        $params['type'] = $event;
        $params['marketplace_type'] = CoinCollector::MP_TYPE;
        $params['name'] = $params['ProductTitle'];
        $params['message'] = null;
        $params['expiration_date'] = null;
        $params['link'] = null;
        $params['quantity'] = 1;

        $error_code = 0;
        $params['status'] = 'in_progress';
        $count_limit = 50;

        if ($event == 'delete') {
            $params['quantity'] = 0;
        }

        if (!empty($response->faultstring)) {
            $cc_status = 'Error';
            $params['message'] = $response->faultstring;
        } else {
            switch ($event) {
                case 'create':
                case 'update':
                    if ($response->InsertCoinListingByCriteriaResult->Success) {
                        $cc_status = 'Success';
                    } else {
                        $cc_status = 'Error';
                        $error_code = $response->InsertCoinListingByCriteriaResult->ResponseNumber;
                        $params['message'] = $response->InsertCoinListingByCriteriaResult->ResponseDescription;
                    }
                    break;
                case 'delete':
                    if ($response->DeleteCoinListingByDPNResult->Success) {
                        $cc_status = 'Success';
                    } else {
                        $cc_status = 'Error';
                        $error_code = $response->DeleteCoinListingByDPNResult->ResponseNumber;
                        $params['message'] = $response->DeleteCoinListingByDPNResult->ResponseDescription;
                    }
                    break;
                default:
                    $cc_status = 'Error';
                    break;
            }
        }

        if ($cc_status == 'Error') {
            $report_error = ReportError::where('message', $params['message'])->first();
            if ($report_error) {
                $report_error->increment('count');
                $report_error->save();
                if ($report_error->count >= $count_limit) {
                    $params['status'] = 'error';
                }
            } else {
                ReportError::create([
                    'error_code' => $error_code,
                    'message' => $params['message'],
                    'mp_type' => CoinCollector::MP_TYPE,
                ]);
            }
        }

        if ($cc_status == 'Success') {
            $params['message'] = 'Item was successfully ' . $event . 'd.';
            $params['status'] = 'active';
            if ($event == 'delete') {
                if ($report) {
                    $report->delete();
                    return $cc_status;
                }
            }
        }

        if ($report) {
            $report->type = $report->type === 'delete' ? 'delete' : $params['type'];
            $report->status = $params['status'];
            $report->message = $params['message'];
            $report->name = $params['name'];
            $report->quantity = $params['quantity'];
            $report->link = $params['link'];
            $report->expiration_date = $params['expiration_date'];
            $report->save();
        } else {
            Report::create($params);
        }

        return $cc_status;
    }

    private static function fillParam(Product $item)
    {
        $CC_mint = ['C', 'CC', 'D', 'O', 'P', 'S', 'W'];
        $CC_mspr = ['MS', 'PL', 'PR', 'SP'];
        $CC_metalls = ['Aluminum', 'Billon', 'Brass', 'Copper', 'Copper-Nickel',
                    'German-Silver', 'Gold', 'Nickel', 'Other', 'Silver',
                    'Steel', 'Tin', 'White Metal'];

        $settings = MarketplaceItemSetting::where([
                        'product_id' => $item->id,
                        'marketplace' => CoinCollector::MP_TYPE
                    ])->first();
        $price = number_format($item->sell_premium * (1 + $settings->pricing / 100), 2, '.', '');

        // ProductTitle
        $parent_name = Product::find($item->parent_id)->name;
        $title = Marketplace::individualTitle(
            $item->num_year,
            $item->num_mint_mark,
            $parent_name,
            $item->num_grading_agency,
            $item->num_grade,
            $item->num_variety,
            $item->num_designation,
            false
        );

        // ImageURL
        $image = ['', '', '', ''];
        $url = env('APP_URL');
        $product_imgs = ProductImage::where('product_id', $item->id)->get();
        foreach ($product_imgs as $product_img_index => $product_img) {
            $image[$product_img_index] = $url . "/media/" . $product_img->path;
            if($product_img_index >= 4) {
                break;
            }
        }

        // GradingService
        $grading_service = '';
        if ($item->num_grading_agency !== 'Not certified') {
            $grading_service = $item->num_grading_agency;
        }

        // Grade
        $grade = '';
        $num_grade = substr($item->num_grade, 3);
        if ((int)$num_grade > 0 ) {
            if (substr($num_grade, -1) == '+' || substr($num_grade, -2, 1) == '+') {
                $grade = (int)$num_grade . '+';
            } else {
                $grade = strval((int)$num_grade);
            }
        }

        // CategoryCode
        $product_categories = DB::table('products_red_book_categories')->where('product_id', $item->parent_id)->get();
        $count_product_categories = count($product_categories);

        $parent_category_id = null;
        switch ($count_product_categories) {
            case 1:
                $parent_category_id = $product_category->red_book_category_id;
                break;
            case 2:
            case 3:
                foreach ($product_categories as $product_category) {
                    if ($parent_category_id < $product_category->red_book_category_id) {
                        $parent_category_id = $product_category->red_book_category_id;
                    }
                }
                break;
            default:
                break;
        }

        $category_code = '';
        if (!empty($parent_category_id)) {
            $category_code = RedBookCategory::find($parent_category_id)->CC_code;
        }

        // Mint
        $mint = '';
        if (in_array($item->num_mint_mark, $CC_mint)) {
            $mint = $item->num_mint_mark;
        }

        // Metal
        $metall = 'Other';
        $material_name = Material::find($item->material_id)->name;
        if (in_array($material_name, $CC_metalls)) {
            $metall = $material_name;
        }

        // MSPR
        $mspr = '';
        if (in_array($item->num_strike, $CC_mspr)) {
            $mspr = $item->num_strike;
        }

        $params = [
            'DealerProductNumber' => strval($item->id),
            'RetailPrice' => $price,
            'WholesalePrice' => $price,
            'Quantity' => 1,
            'ProductTitle' => $title,
            'ProductDescription' => $item->shopping_cart_description,
            'ImageURL1' => $image[0] ? $image[0] : '',
            'ImageURL2' => $image[1] ? $image[1] : '',
            'GradingService' => $grading_service,
            'Grade' => $grade,
            'CategoryCode' => $category_code,
            'YearIssued' => strval($item->num_year),
            'Mint' => $mint,
            'Metal' => $metall,
            'MSPR' => $mspr,
            'Attribute' => '',
        ];

        return $params;
    }
}
