<?php

namespace ppm\Models;

use Illuminate\Database\Eloquent\Model;

class ReviewVote extends Model
{
    protected $table = 'review_votes';

    protected $fillable = [
        'review_id',
        'user_id',
        'ip_address',
        'useragent_hash'
    ];

    protected $casts = [
        'review_id' => 'integer',
        'user_id'    => 'integer'
    ];

    public function review()
    {
        return $this->belongsTo(Review::class);
    }

    public function save(array $options = [ ])
    {
        $this->requestUserAgentHash();

        return parent::save($options);
    }

    public function requestUserAgentHash()
    {
        if (array_get($this->attributes, 'useragent_hash')) {
            return;
        }

        $this->attributes['useragent_hash'] = sha1('User-Agent');

        return $this->attributes['useragent_hash'];
    }
}
