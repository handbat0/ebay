<?php

namespace ppm\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use ppm\Models\Products\Product;
use ppm\Models\Users\Client;

class Review extends Model
{
    public $timestamps = false;

    protected $table = 'reviews';

    protected $fillable = [
        'product_id',
        'user_id',
        'date',
        'rating',
        'title',
        'content',
        'votes'
    ];

    protected $casts = [
        'product_id' => 'integer',
        'user_id'    => 'integer',
        'votes'      => 'integer',
    ];

    protected $dates = ['date'];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }


    public function user()
    {
        return $this->belongsTo(Client::class, 'user_id');
    }

    public function reviewVotes()
    {
        return $this->hasMany(ReviewVote::class);
    }

    public function comments()
    {
        return $this->hasMany(ReviewComment::class);
    }

    public function save(array $options = [ ])
    {
        $this->setDateForCurrentDateTime();

        return parent::save($options);
    }

    public function setDateForCurrentDateTime()
    {
        if (array_get($this->attributes, 'date')) {
            return;
        }

        $this->attributes['date'] = Carbon::now()->toDateTimeString();

        return $this->attributes['date'];
    }

    public function getUserNameAttribute()
    {
        return $this->user->name;
    }
}
