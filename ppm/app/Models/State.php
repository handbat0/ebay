<?php
namespace ppm\Models;

class State
{
    protected static $list;

    public static function all()
    {
        if (!static::$list) {
            static::$list = collect(json_decode(file_get_contents(resource_path('/data/states.json')), true));
        }

        return static::$list;
    }

    public static function findByCode($code)
    {
        return static::all()->where('abbreviation', $code)->first();
    }
}
