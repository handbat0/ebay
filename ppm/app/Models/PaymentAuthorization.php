<?php

namespace ppm\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use ppm\Models\Orders\Order;
use ppm\Models\Users\Client;

class PaymentAuthorization extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'client_id',
        'card_id',
        'amount',
        'payment_provider',
        'token',
        'payment_id',
        'order_id',
        'capture_date'
    ];

    protected $casts = [
        'client_id'  => 'integer',
        'card_id'    => 'integer',
        'amount'     => 'double',
        'payment_id' => 'integer',
        'order_id'   => 'integer'
    ];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function payment()
    {
        return $this->belongsTo(Payment::class);
    }
}
