<?php

namespace ppm\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use ppm\Models\Products\Location;

class TaxRate extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_type', 'location_id', 'tax_rate', 'start_date', 'end_date'
    ];

    protected $casts = [
        'location_id' => 'integer',
        'tax_rate'    => 'decimal'
    ];

    public static function getTaxRatesApplicableAt($date)
    {
        /** @var Collection $rates */
        $rates = self::availableAt($date)->get();
        return $rates->reduce(function ($acc, $item) {
            $acc[sprintf('%s-%s', $item->location_id, $item->product_type)] = $item->tax_rate;
            return $acc;
        }, []);
    }

    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    public function scopeAvailableAt(Builder $query, $date)
    {
        return $query->where('start_date', '<=', $date)
                    ->where('end_date', '>', $date);
    }

    public function save(array $options = [])
    {
        $this->setStartDateForToday();
        $this->setEndDateForASpecificDate();

        return parent::save($options);
    }

    public function setStartDateForToday()
    {
        if (array_get($this->attributes, 'start_date')) {
            return;
        }

        $this->attributes['start_date'] = Carbon::now()->toDateTimeString();

        return $this->attributes['start_date'];
    }

    public function setEndDateForASpecificDate()
    {
        if (array_get($this->attributes, 'end_date')) {
            return;
        }

        $this->attributes['end_date'] = '9999-12-31';

        return $this->attributes['end_date'];
    }
}
