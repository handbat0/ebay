<?php

namespace Tests;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Eloquent\Model;

use \DTS\eBaySDK\Constants;
use \DTS\eBaySDK\Trading\Services;
use \DTS\eBaySDK\Trading\Types;

use ppm\Models\Products\Product;
use ppm\Models\Marketplaces\eBay;
use ppm\Marketplace;
use ppm\Report;

use ppm\CreateEbayProduct;
use ppm\UpdateEbayProductQuantity;
use ppm\UpdateEbayProduct;
use ppm\FindEbayProduct;
use ppm\EndEbayProduct;


class eBayTest extends DbTestCase
{
    protected $params = [];

    public function setUp()
    {
        parent::setUp();

        $this->params["credentials"] = [
            "appId"  => "IvanKhan-aurestes-SBX-5dfe99f22-fc107df8",
            "certId" => "SBX-dfe99f226946-43f4-40f5-8543-33ae",
            "devId"  => "84ae2671-505c-4cdd-b5aa-ab1199939a47",
            "eBayAuthToken" => "AgAAAA**AQAAAA**aAAAAA**/iBuXQ**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4aiCZeHpgSdj6x9nY+seQ**aRQFAA**AAMAAA**UahAH0s7ezCk2qp660rsrxonHgaUaOz9x7R7odLxn5RNIXJvy0He0smOQR+uOp/Jp0/ERC/+plhEcFmuxoUQPAY+jdLWUxWm9oK3QCjvLjwczts/BY2LCjud1giXTSFc991CJ0wIUSvc6i7IyIH69AdF1zVxv1ceQM1FwH1yQmdLjiLn8+3imvPdmtVesD0jzMZVZ3Y2olA05MhkHYx9RbVEuwDStI2THYC+IQ6ZhDdIYmATg6HxcjpAB1FXPyfBLi52NW5vkEzxZBMk7aRY8w8qERSWq4dEQ+raGWZcbCYlr6bIHXI8DRRBnilyDPB8nBPAIOdHCqLHHGLKM/LErON9e5sVXsoYfsYDrEdrSUDVtd4Qsne5rWrs1S5Qt3IfUptZ+4ouARNfOfecYoKETjRmbviBFAHne25tHsOtAUzKPpOCZtKXK6RxejoTMp1kOHmuxzMH47f4XfOmJJOABEux9tkl+lwmLpp7MtgonmlEkZa/tWtOBASMPlHb6aM1Au9lqGtfIk+3RkCDYTBDCOtHsaauhKx3kEbogikrQ1qFKilytEKIyZykr6I05erlaWBclw4GecZW1lhdzZhc0PMerTknTlQmEnhdshC4JthdOfHiLSZtjObVVcYdlD1maDbJ2Nlel1BAs6JPMl+oPEcHy3Vw/Mk9W6c8eBhF78GkNbtmijGKS0bbY/UX/fnugqgwE0Mmyrs0I11jERazwGpzn343rNl07G0eu4NMnyvhS0Fu/MKiuGWLm930zWOE"
        ];
    }

    public function testGetTime()
    {
        // invalid eBayAuthToken
        $this->params["credentials"]["eBayAuthToken"] = "AgAAAA**AQAAAA**aAAAAA**/iBuXQ**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4aiCZeHpgSdj6x9nY+seQ**aRQFAA**AAMAAA**UahAH0s7ezCk2qp660rsrxonHgaUaOz9x7R7odLxn5RNIXJvy0He0smOQR+uOp/Jp0/ERC/+plhEcFmuxoUQPAY+jdLWUxWm9oK3QCjvLjwczts/BY2LCjud1giXTSFc991CJ0wIUSvc6i7IyIH69AdF1zVxv1ceQM1FwH1yQmdLjiLn8+3imvPdmtVesD0jzMZVZ3Y2olA05MhkHYx9RbVEuwDStI2THYC+IQ6ZhDdIYmATg6HxcjpAB1FXPyfBLi52NW5vkEzxZBMk7aRY8w8qERSWq4dEQ+raGWZcbCYlr6bIHXI8DRRBnilyDPB8nBPAIOdHCqLHHGLKM/LErON9e5sVXsoYfsYDrEdrSUDVtd4Qsne5rWrs1S5Qt3IfUptZ+4ouARNfOfecYoKETjRmbviBFAHne25tHsOtAUzKPpOCZtKXK6RxejoTMp1kOHmuxzMH47f4XfOmJJOABEux9tkl+lwmLpp7MtgonmlEkZa/tWtOBASMPlHb6aM1Au9lqGtfIk+3RkCDYTBDCOtHsaauhKx3kEbogikrQ1qFKilytEKIyZykr6I05erlaWBclw4GecZW1lhdZhc0PMerTknTlQmEnhdshC4JthdOfHiLSZtjObVVcYdlD1maDbJ2Nlel1BAs6JPMl+oPEcHy3Vw/Mk9W6c8eBhF78GkNbtmijGKS0bbY/UX/fnugqgwE0Mmyrs0I11wGpzn343rNl07G0eu4NMnyvhS0Fu/MKiuGWLm930zWOE";
        $response = eBay::getEbayTime($this->params["credentials"]);
        $this->assertEquals(false, $response);

        // valid eBayAuthToken
        $this->params["credentials"]["eBayAuthToken"] = "AgAAAA**AQAAAA**aAAAAA**/iBuXQ**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4aiCZeHpgSdj6x9nY+seQ**aRQFAA**AAMAAA**UahAH0s7ezCk2qp660rsrxonHgaUaOz9x7R7odLxn5RNIXJvy0He0smOQR+uOp/Jp0/ERC/+plhEcFmuxoUQPAY+jdLWUxWm9oK3QCjvLjwczts/BY2LCjud1giXTSFc991CJ0wIUSvc6i7IyIH69AdF1zVxv1ceQM1FwH1yQmdLjiLn8+3imvPdmtVesD0jzMZVZ3Y2olA05MhkHYx9RbVEuwDStI2THYC+IQ6ZhDdIYmATg6HxcjpAB1FXPyfBLi52NW5vkEzxZBMk7aRY8w8qERSWq4dEQ+raGWZcbCYlr6bIHXI8DRRBnilyDPB8nBPAIOdHCqLHHGLKM/LErON9e5sVXsoYfsYDrEdrSUDVtd4Qsne5rWrs1S5Qt3IfUptZ+4ouARNfOfecYoKETjRmbviBFAHne25tHsOtAUzKPpOCZtKXK6RxejoTMp1kOHmuxzMH47f4XfOmJJOABEux9tkl+lwmLpp7MtgonmlEkZa/tWtOBASMPlHb6aM1Au9lqGtfIk+3RkCDYTBDCOtHsaauhKx3kEbogikrQ1qFKilytEKIyZykr6I05erlaWBclw4GecZW1lhdzZhc0PMerTknTlQmEnhdshC4JthdOfHiLSZtjObVVcYdlD1maDbJ2Nlel1BAs6JPMl+oPEcHy3Vw/Mk9W6c8eBhF78GkNbtmijGKS0bbY/UX/fnugqgwE0Mmyrs0I11jERazwGpzn343rNl07G0eu4NMnyvhS0Fu/MKiuGWLm930zWOE";
        $response = eBay::getEbayTime($this->params["credentials"]);
        $this->assertEquals(true, $response);
    }

    public function testIndividualNumismaticProduct()
    {
        \Artisan::call('migrate:fresh');
        $this->seedEbayKeys();

        $certificate_arr = ["PCGS", "NGC", "ANACS", "ICG"];
        $cac_arr = ["None", "CAC", "CAC Gold"];
        $strike_arr = ["MS", "PR", "SP"];
        $category_arr = ["173592", "11947", "31373", "39458", "11964", "173587", "162166", "11966", "45142", "3390"];
        $strike = $strike_arr[rand(0,2)];

        $individual = factory(Product::class)->states('numismatic')->create([
            'country'                      => 'USA',
            'num_year'                     => strval(rand(1000,2000)),
            'entry_type'                   => 'individual',
            'num_mint_mark'                => "mint location " . rand(1, 1000),
            'num_grade'                    => $strike."-70",
            'num_strike'                   => $strike,
            'total_stock'                  => 1,
            'parent_id'                    => factory(Product::class)->states('numismatic')->create([
                'entry_type'                   => 'individual',
            ])->id,
            'price'                        => 1,
            'sell_premium'                 => rand(1, 1000),
            'sell_price_method'            => 'fixed',
        ]);

        // individual id
        $this->assertNotNull($individual->id);
        $this->params["product_id"] = $individual->id;

        $this->params["entry_type"] = 'individual';
        $this->params["title"] = "UnitTest individual Numismatic " . rand(1, 1000);
        $this->params["category_id"] = $category_arr[rand(0,9)];
        $this->params["description"] = "description " . $individual->shopping_cart_description;
        $this->params["quantity"] = $individual->total_stock;

        $this->params["grading_service"] = $certificate_arr[rand(0,3)];
        $this->params["composition"] = "composition " . rand(1, 1000);
        $this->params["grade"] = $individual->num_grade;
        $this->params["year"] = $individual->num_year;
        $this->params["strike_type"] = $individual->num_strike;
        $this->params["mint_location"] = $individual->num_mint_mark;
        $this->params["country"] = $individual->country;

        $this->params["location_country"] = "US";
        $this->params["address"] = "Los Angeles";
        $this->params['postal'] = 90802;

        $num_spec_prop = $cac_arr[rand(0,2)];

        if ($this->params["grading_service"] == "PCGS" && $num_spec_prop == "None") {
            $this->params["certification"] = $this->params["grading_service"];
        } elseif ($this->params["grading_service"] == "PCGS" && $num_spec_prop !== "None") {
            $this->params["certification"] = "PCGS and CAC";
        } elseif ($this->params["grading_service"] == "NGC" && $num_spec_prop == "None") {
            $this->params["certification"] = $this->params["grading_service"];
        } elseif ($this->params["grading_service"] == "NGC" && $num_spec_prop!== "None") {
            $this->params["certification"] = "NGC and CAC";
        } else {
            $this->params["certification"] = $this->params["grading_service"];
        }

        $this->getEbayResult();
    }

    public function testCreateBulkNumismaticProduct()
    {
        \Artisan::call('migrate:fresh');
        $this->seedEbayKeys();

        $certificate_arr = ["PCGS", "", "NGC"];
        $category_arr = ["173592", "11947", "31373", "39458", "11964", "173587", "162166", "11966", "45142", "3390"];

        $bulk = factory(Product::class)->states('numismatic')->create([
            'entry_type'                   => 'bulk',
            'total_stock'                  => 5,
            'price'                        => rand(1,99),
            'sell_premium'                 => rand(100,999),
            'sell_price_method'            => 'fixed',
        ]);

        // bulk id
        $this->assertNotNull($bulk->id);
        $this->params["product_id"] = $bulk->id;

        $this->params["entry_type"] = 'bulk';
        $this->params["title"] = 'UnitTest bulk '.rand(0,999).': certificate - '. $certificate_arr[rand(0,2)];
        $this->params["quantity"] = $bulk->total_stock;
        $this->params["price"] = $bulk->sell_premium;
        $this->params["category_id"] = $category_arr[rand(0,9)];
        $this->params["description"] = "<p>".$bulk->shopping_cart_description."</p>";

        $this->params["location_country"] = "US";
        $this->params["address"] = "Los Angeles";
        $this->params['postal'] = 90802;

        if (strpos($this->params["title"], "PCGS") !== false) {
            $this->params["certification"] = "PCGS";
        } elseif (strpos($this->params["title"], "NGC") !== false) {
            $this->params["certification"] = "NGC";
        } else {
            if ($this->params["category_id"] == "162166") {
                $this->params["certification"] = "Uncertified";
            } else {
                $this->params["certification"] = "U.S. Mint";
            }
        }

        $this->getEbayResult();
    }

    protected function getService()
    {
        $service = new Services\TradingService([
            'siteId' => Constants\SiteIds::US,
            'sandbox' => env('SANDBOX'),
            'credentials' => [
                'appId'  => $this->params["credentials"]["appId"],
                'certId' => $this->params["credentials"]["certId"],
                'devId'  => $this->params["credentials"]["devId"]
            ]
        ]);
        return $service;
    }

    protected function getRequest()
    {
        $request = new Types\GetMyeBaySellingRequestType();
        $request->RequesterCredentials = new Types\CustomSecurityHeaderType();
        $request->RequesterCredentials->eBayAuthToken = $this->params["credentials"]["eBayAuthToken"];

        return $request;
    }

    protected function seedEbayKeys()
    {
        Marketplace::create([
            'type' => 'eBay',
            'api_token' => $this->params["credentials"]["eBayAuthToken"],
            'app_id' => $this->params["credentials"]["appId"],
            'cert_id' => $this->params["credentials"]["certId"],
            'dev_id' => $this->params["credentials"]["devId"],
        ]);
    }

    public function getEbayResult()
    {
        // ------------------------------ CREATE ------------------------------//
        // invalid create
        $this->params["price"] = 0.01;
        $invalidCoin = CreateEbayProduct::CreateProduct($this->params);
        $this->assertEquals("Failure", $invalidCoin->Ack);

        $invalidCreateReport = eBay::report($invalidCoin, 'create', $this->params)['status'];
        $invalidReport = Report::where('product_id', $this->params["product_id"])->first();
        $this->assertEquals("in_progress", $invalidReport->status);

        // valid create
        $this->params["price"] = 100;
        $validCoin = CreateEbayProduct::CreateProduct($this->params);
        $this->assertEquals("Success", $validCoin->Ack);

        $validCreateReport = eBay::report($validCoin, 'create', $this->params)['status'];
        $validReport = Report::where('product_id', $this->params["product_id"])->first();
        $this->assertEquals("active", $validReport->status);
        // ebay_id
        $this->params["ebay_id"] = $validCoin->ItemID;

        // ------------------------------ FIND ------------------------------//
        $find = FindEbayProduct::FindProduct($this->params["ebay_id"], $this->params["credentials"]);
        $this->assertEquals("Success", $find->Ack);

        // ------------------------------ UPDATE ------------------------------//
        // invalid update
        $this->params["title"] = "new " . $this->params["title"];
        $this->params["price"] = 0.01;

        $invalidUpdateCoin = UpdateEbayProduct::UpdateProduct($this->params);
        $this->assertEquals("Failure", $invalidUpdateCoin->Ack);

        $invalidUpdateReport = eBay::report($invalidUpdateCoin, 'update', $this->params)['status'];
        $invalidReport = Report::where('product_id', $this->params["product_id"])->first();
        $this->assertEquals("in_progress", $invalidReport->status);
        $this->assertEquals("update", $invalidReport->type);

        // valid update
        $this->params["price"] = 100;
        if ($this->params["entry_type"] == 'bulk') {
            $this->params["quantity"] += 100;
        }

        $validUpdateCoin = UpdateEbayProduct::UpdateProduct($this->params);
        $this->assertEquals("Success", $validUpdateCoin->Ack);

        $validUpdateReport = eBay::report($validUpdateCoin, 'update', $this->params)['status'];
        $validReport = Report::where('product_id', $this->params["product_id"])->first();
        $this->assertEquals("active", $validReport->status);
        $this->assertEquals("update", $validReport->type);

        // ------------------------------ DELETE ------------------------------//
        $delete = EndEbayProduct::EndProduct($this->params["ebay_id"], $this->params["credentials"]);
        $this->assertEquals("Success", $delete->Ack);

        $validDeleteReport = eBay::report($delete, 'delete', $this->params)['status'];
        $validReport = Report::where('product_id', $this->params["product_id"])->first();
        $this->assertNull($validReport);
    }

}
