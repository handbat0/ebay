<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use \DTS\eBaySDK\Constants;
use \DTS\eBaySDK\Trading\Services;
use \DTS\eBaySDK\Trading\Types;

use ppm\UpdateEbayProduct;

class eBay_05_UpdateProductTest extends TestCase
{
    protected $params = [];

    public function testUpdateUniqueNumismaticProduct()
    {
        $this->params["credentials"] = [
            "appId"  => "IvanKhan-aurestes-SBX-5dfe99f22-fc107df8",
            "certId" => "SBX-dfe99f226946-43f4-40f5-8543-33ae",
            "devId"  => "84ae2671-505c-4cdd-b5aa-ab1199939a47",
            "eBayAuthToken" => "AgAAAA**AQAAAA**aAAAAA**/iBuXQ**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4aiCZeHpgSdj6x9nY+seQ**aRQFAA**AAMAAA**UahAH0s7ezCk2qp660rsrxonHgaUaOz9x7R7odLxn5RNIXJvy0He0smOQR+uOp/Jp0/ERC/+plhEcFmuxoUQPAY+jdLWUxWm9oK3QCjvLjwczts/BY2LCjud1giXTSFc991CJ0wIUSvc6i7IyIH69AdF1zVxv1ceQM1FwH1yQmdLjiLn8+3imvPdmtVesD0jzMZVZ3Y2olA05MhkHYx9RbVEuwDStI2THYC+IQ6ZhDdIYmATg6HxcjpAB1FXPyfBLi52NW5vkEzxZBMk7aRY8w8qERSWq4dEQ+raGWZcbCYlr6bIHXI8DRRBnilyDPB8nBPAIOdHCqLHHGLKM/LErON9e5sVXsoYfsYDrEdrSUDVtd4Qsne5rWrs1S5Qt3IfUptZ+4ouARNfOfecYoKETjRmbviBFAHne25tHsOtAUzKPpOCZtKXK6RxejoTMp1kOHmuxzMH47f4XfOmJJOABEux9tkl+lwmLpp7MtgonmlEkZa/tWtOBASMPlHb6aM1Au9lqGtfIk+3RkCDYTBDCOtHsaauhKx3kEbogikrQ1qFKilytEKIyZykr6I05erlaWBclw4GecZW1lhdzZhc0PMerTknTlQmEnhdshC4JthdOfHiLSZtjObVVcYdlD1maDbJ2Nlel1BAs6JPMl+oPEcHy3Vw/Mk9W6c8eBhF78GkNbtmijGKS0bbY/UX/fnugqgwE0Mmyrs0I11jERazwGpzn343rNl07G0eu4NMnyvhS0Fu/MKiuGWLm930zWOE"
        ];

        $service = new Services\TradingService([
            'siteId' => Constants\SiteIds::US,
            'sandbox' => env('SANDBOX'),
            'credentials' => [
                'appId'  => $this->params["credentials"]["appId"],
                'certId' => $this->params["credentials"]["certId"],
                'devId'  => $this->params["credentials"]["devId"]
            ]
        ]);

        $request = new Types\GetMyeBaySellingRequestType();
        $request->RequesterCredentials = new Types\CustomSecurityHeaderType();
        $request->RequesterCredentials->eBayAuthToken = $this->params["credentials"]["eBayAuthToken"];

        $request->ActiveList = new Types\ItemListCustomizationType();
        $request->ActiveList->Include = true;

        $response = $service->getMyeBaySelling($request);

        foreach ($response->ActiveList->ItemArray->Item as $item) {
            if (strpos($item->Title, "UnitTest individual") !== false) {
                $this->params["ebay_id"] = strval($item->ItemID);
                break;
            }
        }

        $certificate_arr = ["PCGS", "NGC", "ANACS", "ICG"];
        $cac_arr = ["None", "CAC", "CAC Gold"];
        $strike_arr = ["MS", "PR", "SP"];
        $category_arr = ["173592", "11947", "31373", "39458", "11964", "173587", "162166", "11966", "45142", "3390"];
        $strike = $strike_arr[rand(0,2)];

        //$this->params["id"] = "110446459734";
        $this->params["title"] = "new UnitTest individual " . rand(1, 1000);
        $this->params["price"] = rand(1, 1000);
        $this->params["category_id"] = $category_arr[rand(0,9)];
        $this->params["description"] = "description " . rand(1, 1000);

        $this->params["grading_service"] = $certificate_arr[rand(0,3)];
        $this->params["composition"] = "composition " . rand(1, 1000);
        $this->params["grade"] = $strike."-70";
        $this->params["year"] = strval(rand(1,9999));
        $this->params["strike_type"] = $strike;
        $this->params["mint_location"] = "mint location " . rand(1, 1000);
        $this->params["country"] = "country " . rand(1, 1000);
        $this->params["quantity"] = 1;

        $num_spec_prop = $cac_arr[rand(0,2)];

        if ($this->params["grading_service"] == "PCGS" && $num_spec_prop == "None") {
            $this->params["certification"] = $this->params["grading_service"];
        } elseif ($this->params["grading_service"] == "PCGS" && $num_spec_prop !== "None") {
            $this->params["certification"] = "PCGS and CAC";
        } elseif ($this->params["grading_service"] == "NGC" && $num_spec_prop == "None") {
            $this->params["certification"] = $this->params["grading_service"];
        } elseif ($this->params["grading_service"] == "NGC" && $num_spec_prop!== "None") {
            $this->params["certification"] = "NGC and CAC";
        } else {
            $this->params["certification"] = $this->params["grading_service"];
        }

        $response = UpdateEbayProduct::UpdateProduct($this->params);

        $this->assertNotEquals("Failure", $response->Ack);
    }

    public function testUpdateBulkNumismaticProduct()
    {
        $this->params["credentials"] = [
            "appId"  => "IvanKhan-aurestes-SBX-5dfe99f22-fc107df8",
            "certId" => "SBX-dfe99f226946-43f4-40f5-8543-33ae",
            "devId"  => "84ae2671-505c-4cdd-b5aa-ab1199939a47",
            "eBayAuthToken" => "AgAAAA**AQAAAA**aAAAAA**/iBuXQ**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4aiCZeHpgSdj6x9nY+seQ**aRQFAA**AAMAAA**UahAH0s7ezCk2qp660rsrxonHgaUaOz9x7R7odLxn5RNIXJvy0He0smOQR+uOp/Jp0/ERC/+plhEcFmuxoUQPAY+jdLWUxWm9oK3QCjvLjwczts/BY2LCjud1giXTSFc991CJ0wIUSvc6i7IyIH69AdF1zVxv1ceQM1FwH1yQmdLjiLn8+3imvPdmtVesD0jzMZVZ3Y2olA05MhkHYx9RbVEuwDStI2THYC+IQ6ZhDdIYmATg6HxcjpAB1FXPyfBLi52NW5vkEzxZBMk7aRY8w8qERSWq4dEQ+raGWZcbCYlr6bIHXI8DRRBnilyDPB8nBPAIOdHCqLHHGLKM/LErON9e5sVXsoYfsYDrEdrSUDVtd4Qsne5rWrs1S5Qt3IfUptZ+4ouARNfOfecYoKETjRmbviBFAHne25tHsOtAUzKPpOCZtKXK6RxejoTMp1kOHmuxzMH47f4XfOmJJOABEux9tkl+lwmLpp7MtgonmlEkZa/tWtOBASMPlHb6aM1Au9lqGtfIk+3RkCDYTBDCOtHsaauhKx3kEbogikrQ1qFKilytEKIyZykr6I05erlaWBclw4GecZW1lhdzZhc0PMerTknTlQmEnhdshC4JthdOfHiLSZtjObVVcYdlD1maDbJ2Nlel1BAs6JPMl+oPEcHy3Vw/Mk9W6c8eBhF78GkNbtmijGKS0bbY/UX/fnugqgwE0Mmyrs0I11jERazwGpzn343rNl07G0eu4NMnyvhS0Fu/MKiuGWLm930zWOE"
        ];

        $service = new Services\TradingService([
            'siteId' => Constants\SiteIds::US,
            'sandbox' => env('SANDBOX'),
            'credentials' => [
                'appId'  => $this->params["credentials"]["appId"],
                'certId' => $this->params["credentials"]["certId"],
                'devId'  => $this->params["credentials"]["devId"]
            ]
        ]);

        $request = new Types\GetMyeBaySellingRequestType();
        $request->RequesterCredentials = new Types\CustomSecurityHeaderType();
        $request->RequesterCredentials->eBayAuthToken = $this->params["credentials"]["eBayAuthToken"];

        $request->ActiveList = new Types\ItemListCustomizationType();
        $request->ActiveList->Include = true;

        $response = $service->getMyeBaySelling($request);

        foreach ($response->ActiveList->ItemArray->Item as $item) {
            if (strpos($item->Title, "UnitTest bulk") !== false) {
                $this->params["ebay_id"] = strval($item->ItemID);
                break;
            }
        }

        $certificate_arr = ["PCGS", "", "NGC"];
        $category_arr = ["173592", "11947", "31373", "39458", "11964", "173587", "162166", "11966", "45142", "3390"];

        $this->params["title"] = 'new UnitTest bulk '.rand(0,999).': certificate - '. $certificate_arr[rand(0,2)];
        $this->params["quantity"] = rand(5, 100);
        $this->params["price"] = rand(1, 1000);
        $this->params["category_id"] = $category_arr[rand(0,9)];
        $this->params["description"] = "<p>shopping_cart_description test - ". rand(0,999)."</p>";

        if (strpos($this->params["title"], "PCGS") !== false) {
            $this->params["certification"] = "PCGS";
        } elseif (strpos($this->params["title"], "NGC") !== false) {
            $this->params["certification"] = "NGC";
        } else {
            if ($this->params["category_id"] == "162166") {
                $this->params["certification"] = "Uncertified";
            } else {
                $this->params["certification"] = "U.S. Mint";
            }
        }

        $response = UpdateEbayProduct::UpdateProduct($this->params);

        $this->assertNotEquals("Failure", $response->Ack);
    }
}
