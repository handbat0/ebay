<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use \DTS\eBaySDK\Constants;
use \DTS\eBaySDK\Trading\Services;
use \DTS\eBaySDK\Trading\Types;

use ppm\FindEbayProduct;

class eBay_04_FindEbayProductTest extends TestCase
{
    public function testFindProduct()
    {
        $this->params["credentials"] = [
            "appId"  => "IvanKhan-aurestes-SBX-5dfe99f22-fc107df8",
            "certId" => "SBX-dfe99f226946-43f4-40f5-8543-33ae",
            "devId"  => "84ae2671-505c-4cdd-b5aa-ab1199939a47",
            "eBayAuthToken" => "AgAAAA**AQAAAA**aAAAAA**/iBuXQ**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4aiCZeHpgSdj6x9nY+seQ**aRQFAA**AAMAAA**UahAH0s7ezCk2qp660rsrxonHgaUaOz9x7R7odLxn5RNIXJvy0He0smOQR+uOp/Jp0/ERC/+plhEcFmuxoUQPAY+jdLWUxWm9oK3QCjvLjwczts/BY2LCjud1giXTSFc991CJ0wIUSvc6i7IyIH69AdF1zVxv1ceQM1FwH1yQmdLjiLn8+3imvPdmtVesD0jzMZVZ3Y2olA05MhkHYx9RbVEuwDStI2THYC+IQ6ZhDdIYmATg6HxcjpAB1FXPyfBLi52NW5vkEzxZBMk7aRY8w8qERSWq4dEQ+raGWZcbCYlr6bIHXI8DRRBnilyDPB8nBPAIOdHCqLHHGLKM/LErON9e5sVXsoYfsYDrEdrSUDVtd4Qsne5rWrs1S5Qt3IfUptZ+4ouARNfOfecYoKETjRmbviBFAHne25tHsOtAUzKPpOCZtKXK6RxejoTMp1kOHmuxzMH47f4XfOmJJOABEux9tkl+lwmLpp7MtgonmlEkZa/tWtOBASMPlHb6aM1Au9lqGtfIk+3RkCDYTBDCOtHsaauhKx3kEbogikrQ1qFKilytEKIyZykr6I05erlaWBclw4GecZW1lhdzZhc0PMerTknTlQmEnhdshC4JthdOfHiLSZtjObVVcYdlD1maDbJ2Nlel1BAs6JPMl+oPEcHy3Vw/Mk9W6c8eBhF78GkNbtmijGKS0bbY/UX/fnugqgwE0Mmyrs0I11jERazwGpzn343rNl07G0eu4NMnyvhS0Fu/MKiuGWLm930zWOE"
        ];

        $service = new Services\TradingService([
            'siteId' => Constants\SiteIds::US,
            'sandbox' => env('SANDBOX'),
            'credentials' => [
                'appId'  => $this->params["credentials"]["appId"],
                'certId' => $this->params["credentials"]["certId"],
                'devId'  => $this->params["credentials"]["devId"]
            ]
        ]);

        $request = new Types\GetMyeBaySellingRequestType();
        $request->RequesterCredentials = new Types\CustomSecurityHeaderType();
        $request->RequesterCredentials->eBayAuthToken = $this->params["credentials"]["eBayAuthToken"];

        $request->ActiveList = new Types\ItemListCustomizationType();
        $request->ActiveList->Include = true;

        $response = $service->getMyeBaySelling($request);

        $id = "0";
        foreach ($response->ActiveList->ItemArray->Item as $item) {
            if ($item->ItemID) {
                $id = strval($item->ItemID);
                break;
            }
        }

        $response = FindEbayProduct::FindProduct($id, $this->params["credentials"]);
        $this->assertNotEquals("Failure", $response->Ack);
    }
}
