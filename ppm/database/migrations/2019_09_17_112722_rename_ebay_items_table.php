<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameEbayItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('marketplace_items', 'marketplace_item_settings');
        Schema::rename('ebay_items', 'marketplace_items');
        Schema::table('marketplace_items', function (Blueprint $table) {
            $table->string('marketplace_type');
            $table->renameColumn('ebay_id', 'marketplace_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('marketplace_items', function (Blueprint $table) {
            $table->renameColumn('marketplace_id', 'ebay_id');
            $table->dropColumn('marketplace_type');
        });
        Schema::rename('marketplace_items', 'ebay_items');
        Schema::rename('marketplace_item_settings', 'marketplace_items');
    }
}
