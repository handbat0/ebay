<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id');
            $table->string('type', 100);
            $table->string('status');
            $table->text('message')->nullable();
            $table->text('data')->nullable();
            $table->string('marketplace_type');
            $table->string('name', 80);
            $table->double('quantity')->default(0);
            $table->date('expiration_date')->nullable();
            $table->integer('retries')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
