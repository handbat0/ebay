<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInExternalMarketplacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('external_marketplaces', function (Blueprint $table) {
            $table->integer('bullion_handling_time')->after('bnum_sh_add_cost')->default(30);
            $table->integer('scrap_handling_time')->after('bullion_handling_time')->default(30);
            $table->integer('gems_handling_time')->after('scrap_handling_time')->default(30);
            $table->integer('inum_handling_time')->after('gems_handling_time')->default(30);
            $table->integer('bnum_handling_time')->after('inum_handling_time')->default(30);
        });

        Schema::table('marketplace_item_settings', function (Blueprint $table) {
            $table->integer('handling_time')->default(30);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('marketplace_item_settings', function (Blueprint $table) {
            $table->dropColumn('handling_time');
        });

        Schema::table('external_marketplaces', function (Blueprint $table) {
            $table->dropColumn('bullion_handling_time');
            $table->dropColumn('scrap_handling_time');
            $table->dropColumn('gems_handling_time');
            $table->dropColumn('inum_handling_time');
            $table->dropColumn('bnum_handling_time');
        });
    }
}
