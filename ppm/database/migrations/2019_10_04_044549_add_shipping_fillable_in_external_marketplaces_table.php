<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShippingFillableInExternalMarketplacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('external_marketplaces', function (Blueprint $table) {
            $table->boolean('bullion_pickup')->after('bnum_handling_time')->default(true);
            $table->boolean('scrap_pickup')->after('bullion_pickup')->default(true);
            $table->boolean('gems_pickup')->after('scrap_pickup')->default(true);
            $table->boolean('inum_pickup')->after('gems_pickup')->default(true);
            $table->boolean('bnum_pickup')->after('inum_pickup')->default(true);

            $table->double('bullion_pickup_cost')->after('bnum_pickup')->default(0.0);
            $table->double('scrap_pickup_cost')->after('bullion_pickup_cost')->default(0.0);
            $table->double('gems_pickup_cost')->after('scrap_pickup_cost')->default(0.0);
            $table->double('inum_pickup_cost')->after('gems_pickup_cost')->default(0.0);
            $table->double('bnum_pickup_cost')->after('inum_pickup_cost')->default(0.0);

            $table->double('bullion_pickup_additional_cost')->after('bnum_pickup_cost')->default(0.0);
            $table->double('scrap_pickup_additional_cost')->after('bullion_pickup_additional_cost')->default(0.0);
            $table->double('gems_pickup_additional_cost')->after('scrap_pickup_additional_cost')->default(0.0);
            $table->double('inum_pickup_additional_cost')->after('gems_pickup_additional_cost')->default(0.0);
            $table->double('bnum_pickup_additional_cost')->after('inum_pickup_additional_cost')->default(0.0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('external_marketplaces', function (Blueprint $table) {
            $table->dropColumn('bullion_pickup');
            $table->dropColumn('scrap_pickup');
            $table->dropColumn('gems_pickup');
            $table->dropColumn('inum_pickup');
            $table->dropColumn('bnum_pickup');

            $table->dropColumn('bullion_pickup_cost');
            $table->dropColumn('scrap_pickup_cost');
            $table->dropColumn('gems_pickup_cost');
            $table->dropColumn('inum_pickup_cost');
            $table->dropColumn('bnum_pickup_cost');

            $table->dropColumn('bullion_pickup_additional_cost');
            $table->dropColumn('scrap_pickup_additional_cost');
            $table->dropColumn('gems_pickup_additional_cost');
            $table->dropColumn('inum_pickup_additional_cost');
            $table->dropColumn('bnum_pickup_additional_cost');
        });
    }
}
