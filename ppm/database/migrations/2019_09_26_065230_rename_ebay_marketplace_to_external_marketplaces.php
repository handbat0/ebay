<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameEbayMarketplaceToExternalMarketplaces extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('ebay_marketplace', 'external_marketplaces');
        Schema::table('external_marketplaces', function (Blueprint $table) {
            $table->string('type')->default('eBay');
            $table->dropColumn('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('external_marketplaces', function (Blueprint $table) {
            $table->unsignedInteger('user_id')->nullable();
            $table->dropColumn('type');
        });
        Schema::rename('external_marketplaces', 'ebay_marketplace');
    }
}
