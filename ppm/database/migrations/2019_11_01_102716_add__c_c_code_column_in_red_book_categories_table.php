<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCCCodeColumnInRedBookCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('red_book_categories', function (Blueprint $table) {
            $table->string('CC_code')->after('ebay_category_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('red_book_categories', function (Blueprint $table) {
            $table->dropColumn('CC_code');
        });
    }
}
