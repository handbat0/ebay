<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShippingFillableInMarketplaceItemSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('marketplace_item_settings', function (Blueprint $table) {
            $table->boolean('pickup')->default(true);
            $table->double('pickup_cost')->default(0.0);
            $table->double('pickup_additional_cost')->default(0.0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('marketplace_item_settings', function (Blueprint $table) {
            $table->dropColumn('pickup');
            $table->dropColumn('pickup_cost');
            $table->dropColumn('pickup_additional_cost');
        });
    }
}
