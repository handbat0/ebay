<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserNameAndUserPwdFieldForexternalMarketplacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('external_marketplaces', function (Blueprint $table) {
            $table->string('u_name')->after('paypal_email')->nullable();
            $table->string('u_pwd')->after('u_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('external_marketplaces', function (Blueprint $table) {
            $table->dropColumn('u_name');
            $table->dropColumn('u_pwd');
        });
    }
}
