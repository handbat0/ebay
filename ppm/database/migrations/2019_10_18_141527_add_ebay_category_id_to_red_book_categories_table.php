<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEbayCategoryIdToRedBookCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('red_book_categories', function (Blueprint $table) {
            $table->unsignedInteger('ebay_category_id')->index()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('red_book_categories', function (Blueprint $table) {
            $table->dropColumn('ebay_category_id');
        });
    }
}
