<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use ppm\Models\RedBookCategory;

class addEbayCategoryIdToRedBookCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $json_string = file_get_contents(base_path('resources/data/ebay_category.json'));
        $data = json_decode($json_string, true);
        foreach ($data as $category) {
            $upper = false;
            $name = null;
            $ebay_category_id = 0;
            if (!empty($category['PCGS Category'])) {
                $name = $category['PCGS Category'];
            }
            if (!empty($category['PCGS Subcategory'])) {
                $name = $category['PCGS Subcategory'];
            }
            if (!empty($category['Country'])) {
                $name = $category['Country'];
                $upper = true;
            }

            if (!empty($category['ebay_category'])) {
                if (ctype_digit($category['ebay_category'])) {
                    $ebay_category_id = (int)$category['ebay_category'];
                }
            }

            if ($ebay_category_id > 0 && $name) {
                strtolower($name);
                $category = RedBookCategory::whereRaw('lower(name) like (?)',["{$name}"])->first();
                if ($category) {
                    if ($upper) {
                        $category->name = strtoupper($category->name);
                    }
                    $category->ebay_category_id = $ebay_category_id;
                    $category->save();
                }
            }
        }

        Model::reguard();
    }
}
