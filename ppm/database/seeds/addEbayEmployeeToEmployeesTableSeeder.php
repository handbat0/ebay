<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use ppm\Models\Role;
use ppm\Models\User;

class addEbayEmployeeToEmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        if (!User::where('email', 'eBay@aureuspos.com')->first()) {
            $user = User::create([
                'type' => 'Employee',
                'first_name' => 'eBay',
                'last_name' => '',
                'email' => 'eBay@aureuspos.com',
                'password' => 'wzuTaKlpf2S!I^E',
                'phone' => '',
                'alternate_phone' => '',
                'status' => 'Active',
                'location_id' => 1,
            ]);
            $user->roles()->attach(Role::where('name', 'Employee')->first());
        }

        Model::reguard();
    }
}
