<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use ppm\Models\RedBookCategory;

class AddMenuPositionPCGSCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $json_string = file_get_contents(base_path('resources/data/PCGS_category_position.json'));
        $data = json_decode($json_string, true);
        foreach ($data as $category) {
            $name = null;

            if (!empty($category['category'])) {
                $name = $category['category'];
            }
            if (!empty($category['subcategory'])) {
                $name = $category['subcategory'];
            }

            if (!empty($category['position'])) {
                $position = (int)$category['position'];
            }

            if ($position > 0 && $name) {
                strtolower($name);
                $category = RedBookCategory::whereRaw('lower(name) like (?)',["{$name}"])->first();
                if ($category) {
                    $category->position = $position;
                    $category->save();
                }
            }
        }

        Model::reguard();
    }
}
