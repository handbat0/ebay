<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use ppm\Models\RedBookCategory;

class AddCoinCollectorCategoryCodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $json_string = file_get_contents(base_path('resources/data/CC_code_categories.json'));
        $data = json_decode($json_string, true);
        foreach ($data as $category) {
            $name = null;
            $CC_code = null;

            if (!empty($category['category'])) {
                $name = $category['category'];
            }
            if (!empty($category['subcategory'])) {
                $name = $category['subcategory'];
            }
            if (!empty($category['CC_code'])) {
                $CC_code = $category['CC_code'];
            }

            if ($CC_code && $name) {
                strtolower($name);
                $category = RedBookCategory::whereRaw('lower(name) like (?)',["{$name}"])->first();
                if ($category) {
                    $category->CC_code = $CC_code;
                    $category->save();
                }
            }
        }

        Model::reguard();
    }
}
